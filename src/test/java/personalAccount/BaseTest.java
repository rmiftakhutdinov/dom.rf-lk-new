package personalAccount;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import personalAccount.framework.DriverManager;
import personalAccount.framework.helpers.BaseHelper;
import personalAccount.framework.helpers.BrowserHelper;
import personalAccount.framework.helpers.NavigationHelper;

public class BaseTest {

    protected WebDriver driver;

    private BaseHelper baseHelper;
    private BrowserHelper browserHelper;
    private NavigationHelper navigationHelper;


    // test manage

    @BeforeSuite
    public void beforeSuite(){
        System.out.println("BeforeSuite method is starting!");
    }

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        System.out.println("BeforeTest method is starting!");
        System.out.println();

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser); // webDriver initialization
        browser().maximize();
    }

    @BeforeMethod
    public void beforeMethod(){
        System.out.println();
        System.out.println("BeforeMethod from BaseTest.class");
        System.out.println();
    }

    @AfterMethod
    public void afterMethod(){
        System.out.println();
        System.out.println("AfterMethod from BaseTest.class");
    }

    @AfterTest()
    public void afterTest(){
        System.out.println("AfterTest method is starting!");

        if (Configuration.closeDriverAfterTests){
            if(driver != null){
                driver.close();
            }
        }
    }

    @AfterSuite
    public void afterSuite(){
        System.out.println("AfterSuite method is starting!");
    }


    // helpers initializations

    public BaseHelper action(){
        if (baseHelper == null){
            return new BaseHelper(driver);
        } else {
            return baseHelper;
        }
    }

    public NavigationHelper goTo(){
        if (navigationHelper == null)
        {
            return new NavigationHelper(driver);
        } else {
            return navigationHelper;
        }
    }

    public BrowserHelper browser(){
        if (browserHelper == null)
        {
            return new BrowserHelper(driver);
        } else {
            return browserHelper;
        }
    }

}
