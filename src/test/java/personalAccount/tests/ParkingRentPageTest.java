package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.ParkingRentPage;

public class ParkingRentPageTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Перейти в раздел 'Прочие услуги' из раздела 'Аренда машиноместа'")
    public void goToOtherServicesFromRentParking(){

        ParkingRentPage page = new ParkingRentPage();
        GlobalPage gloPage = new GlobalPage();

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.rentParking);

        try {
            action().waitBy(page.rentParkingElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Аренда машиноместа' не найден");
        }

        action().click(page.breadcrumbsServices);

        try {
            action().waitBy(page.otherServicesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Прочие услуги' не найден");
        }

    }

}
