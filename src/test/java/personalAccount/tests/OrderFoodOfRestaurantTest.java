package personalAccount.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.OrderFoodOfRestaurantPage;

public class OrderFoodOfRestaurantTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Хлебные крошки в заказе еды")
    public void breadcrumbsOrderFood(){

        GlobalPage gloPage = new GlobalPage();
        OrderFoodOfRestaurantPage page = new OrderFoodOfRestaurantPage(driver);

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderFood);

        page.findModalElement();

        action().click(page.deliverToApartment);

        page.findMenuRestaurantElement();
        page.findMenuBarElement();

        action().click(page.barTea);

        page.findBreadcrumbsCartElement();

        action().click(page.breadcrumbsCart);

        page.findCheckoutPageElement();

        action().click(page.breadcrumbsMenu);

        page.findMenuBarElement();

        action().click(page.breadcrumbsMainMenu);

        try {
            action().waitBy(gloPage.apart250);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент главной страницы не найден");
        }

    }

    @Test(description = "Добавление товаров в корзину", priority = 1)
    public void addFoodToBasket(){

        GlobalPage gloPage = new GlobalPage();
        OrderFoodOfRestaurantPage page = new OrderFoodOfRestaurantPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        // Заказ еды из ресторана
        action().click(page.orderFood);

        page.findModalElement();

        // Доставка в апартамент
        action().click(page.deliverToApartment);

        page.findMenuRestaurantElement();
        page.findMenuBarElement();

        // Выбор чая
        action().click(page.barTea);

        page.findBreadcrumbsCartElement();

        try {
            action().waitBy(page.barTeaDropDown);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        // Открыть дроп-даун меню
        action().click(page.barTeaDropDown);

        try {
            action().waitBy(page.openDropDownMenu);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        action().click(page.teaMoreHoney);

        WebElement checkboxHoney = action().findElement(By.xpath("//input[@id='addCardCheck1-0-2']"));

        try {
            Assert.assertTrue(checkboxHoney.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Мёд' не отмечен");
        }

        action().click(page.teaMoreLemon);

        WebElement checkboxLemon = action().findElement(By.xpath("//input[@id='addCardCheck1-0-1']"));

        try {
            Assert.assertTrue(checkboxLemon.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Лимон' не отмечен");
        }

        // Закрыть дроп-даун меню
        action().click(page.barTeaDropDown);

        try {
            Assert.assertFalse(action().isElementPresent(page.openDropDownMenu));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дроп-даун меню не свернулось");
        }

        // Выбор чая
        action().click(page.barCoffee);

        try {
            action().waitBy(page.barCoffeeDropDown);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        // Открыть дроп-даун меню
        action().click(page.barCoffeeDropDown);

        try {
            action().waitBy(page.openDropDownMenu);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        action().click(page.coffeeMoreCoco);

        WebElement checkboxCoco = action().findElement(By.xpath("//input[@id='addCardCheck14-0-2']"));

        try {
            Assert.assertTrue(checkboxCoco.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Кокос' не отмечен");
        }

        action().click(page.coffeeMoreVanilla);

        WebElement checkboxVanilla = action().findElement(By.xpath("//input[@id='addCardCheck14-0-4']"));

        try {
            Assert.assertTrue(checkboxVanilla.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Ваниль' не отмечен");
        }

        // Закрыть дроп-даун меню
        action().click(page.barCoffeeDropDown);

        try {
            Assert.assertFalse(action().isElementPresent(page.openDropDownMenu));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дроп-даун меню не свернулось");
        }

        // Выбор сока
        action().click(page.barJuice);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        // Переход в меню кухни
        action().click(page.kitchen);

        page.findMenuKitchenElement();

        // Выбор супа
        action().click(page.kitchenSoup);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        // Выбор бургера
        action().click(page.kitchenBurger);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        // Сумма в хлебной крошке
        String sumBreadcrumbs = action().findElement(page.breadcrumbsCart).getText();

        // Переход в корзину
        action().click(page.submitToCart);

        page.findCheckoutPageElement();

        // Сумма в корзине
        String sumCart = action().findElement(By.xpath("//h3[@class='order-title ng-binding']")).getText().
                replace("В корзине товаров на сумму", "").
                replace("рублей", "").trim();

        try {
            Assert.assertTrue(sumBreadcrumbs.contains(sumCart));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сверяемые суммы не сходятся");
        }

        // Добавление второго чая
        action().click(page.teaInCart);

        try {
            action().waitBy(page.teaCartDropDown);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        // Новая сумма в корзине
        String newSumCart1 = action().findElement(By.xpath("//h3[@class='order-title ng-binding']")).getText().
                replace("В корзине товаров на сумму", "").
                replace("рублей", "").trim();

        try {
            Assert.assertFalse(newSumCart1.contains(sumCart));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сверяемые суммы не разнятся");
        }

        // Открыть дроп-даун меню
        action().click(page.teaCartDropDown);

        try {
            action().waitBy(page.openDropDownMenu);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        try {
            action().waitBy(page.teaMoreTeapot1Element);
            action().waitBy(page.teaMoreTeapot2Element);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элементы заголовков добавок к чаю не найдены");
        }

        // Закрыть дроп-даун меню
        action().click(page.teaCartDropDown);

        try {
            Assert.assertFalse(action().isElementPresent(page.openDropDownMenu));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дроп-даун меню не свернулось");
        }

        // Добавление второго кофе
        action().click(page.coffeeInCart);

        try {
            action().waitBy(page.coffeeCartDropDown);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        // Новая сумма в корзине
        String newSumCart2 = action().findElement(By.xpath("//h3[@class='order-title ng-binding']")).getText().
                replace("В корзине товаров на сумму", "").
                replace("рублей", "").trim();

        try {
            Assert.assertFalse(newSumCart2.contains(newSumCart1));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сверяемые суммы не разнятся");
        }

        // Открыть дроп-даун меню
        action().click(page.coffeeCartDropDown);

        try {
            action().waitBy(page.openDropDownMenu);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        try {
            action().waitBy(page.coffeeMoreCup1Element);
            action().waitBy(page.coffeeMoreCup2Element);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элементы заголовков добавок к кофе не найдены");
        }

        // Закрыть дроп-даун меню
        action().click(page.coffeeCartDropDown);

        try {
            Assert.assertFalse(action().isElementPresent(page.openDropDownMenu));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дроп-даун меню не свернулось");
        }

        // Добавление второго бургера
        action().click(page.burgerInCart);

        try {
            action().waitBy(page.burgerMoreElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Второй бургер не добавлен");
        }

        // Новая сумма в корзине
        String newSumCart3 = action().findElement(By.xpath("//h3[@class='order-title ng-binding']")).getText().
                replace("В корзине товаров на сумму", "").
                replace("рублей", "").trim();

        try {
            Assert.assertFalse(newSumCart3.contains(newSumCart1));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сверяемые суммы не разнятся");
        }

        // Переход в меню ресторана
        action().click(page.breadcrumbsMenu);

        page.findMenuBarElement();

        // Новая сумма в хлебной крошке
        String newSumBreadcrumbs = action().findElement(page.breadcrumbsCart).getText();

        try {
            Assert.assertTrue(newSumBreadcrumbs.contains(newSumCart3));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сверяемые суммы не сходятся");
        }

        // Переход в меню кухни
        action().click(page.kitchen);

        page.findMenuKitchenElement();

        // Переход в меню бара
        action().click(page.bar);

        page.findMenuBarElement();

    }

    @Test(description = "Доставка еды в апартамент", priority = 2)
    public void deliverFoodToApartment(){

        GlobalPage gloPage = new GlobalPage();
        OrderFoodOfRestaurantPage page = new OrderFoodOfRestaurantPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderFood);

        page.findModalElement();

        action().click(page.deliverToApartment);

        page.findMenuRestaurantElement();
        page.findMenuBarElement();

        action().click(page.barTea);

        page.findBreadcrumbsCartElement();

        try {
            action().waitBy(page.barTeaDropDown);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        action().click(page.barTeaDropDown);

        try {
            action().waitBy(page.openDropDownMenu);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        action().click(page.teaMoreHoney);

        WebElement checkboxHoney = action().findElement(By.xpath("//input[@id='addCardCheck1-0-2']"));

        try {
            Assert.assertTrue(checkboxHoney.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Мед' не отмечен");
        }

        action().click(page.teaMoreLemon);

        WebElement checkboxLemon = action().findElement(By.xpath("//input[@id='addCardCheck1-0-1']"));

        try {
            Assert.assertTrue(checkboxLemon.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Лимон' не отмечен");
        }

        action().click(page.barTeaDropDown);

        try {
            Assert.assertFalse(action().isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Дроп-даун меню не свернулось");
        }

        action().click(page.barCoffee);

        try {
            action().waitBy(page.barCoffeeDropDown);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        action().click(page.barCoffeeDropDown);

        try {
            action().waitBy(page.openDropDownMenu);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        action().click(page.coffeeMoreCoco);

        WebElement checkboxCoco = action().findElement(By.xpath("//input[@id='addCardCheck14-0-2']"));

        try {
            Assert.assertTrue(checkboxCoco.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Кокос' не отмечен");
        }

        action().click(page.coffeeMoreVanilla);

        WebElement checkboxVanilla = action().findElement(By.xpath("//input[@id='addCardCheck14-0-4']"));

        try {
            Assert.assertTrue(checkboxVanilla.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Ваниль' не отмечен");
        }

        action().click(page.barCoffeeDropDown);

        try {
            Assert.assertFalse(action().isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Дроп-даун меню не свернулось");
        }

        action().click(page.barJuice);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        action().click(page.kitchen);

        page.findMenuKitchenElement();

        action().click(page.kitchenSoup);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        action().click(page.kitchenBurger);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        action().click(page.submitToCart);

        page.findCheckoutPageElement();

        action().click(page.date);
        action().click(page.chooseDateOrTime);

        action().click(page.time);
        action().click(page.chooseDateOrTime);

        action().sendKeys(page.comment, "Заказ еды 'Доставка в апартамент' выполнен автотестом");

        action().click(page.submit);

        page.findOrderAcceptedElement();
        page.findManagerConfirmOrderElement();

        action().click(page.submit);

        page.findModalElement();

        action().click(page.deliverToApartment);

        page.findMenuRestaurantElement();
        page.findMenuBarElement();

    }

    @Test(description = "Забронировать столик", priority = 3)
    public void reserveTable(){

        GlobalPage gloPage = new GlobalPage();
        OrderFoodOfRestaurantPage page = new OrderFoodOfRestaurantPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderFood);

        page.findModalElement();

        action().click(page.reserveTable);

        try {
            action().waitBy(page.chooseDateAndTimeReserveTableElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы оформления заказа не найден");
        }

        action().click(page.date);
        action().click(page.chooseDateOrTime);

        action().click(page.time);
        action().click(page.chooseDateOrTime);

        action().sendKeys(page.comment, "Заказ еды 'Бронь стола' выполнен автоматестом");

        action().click(page.submitReserveTable);

        page.findManagerConfirmOrderElement();

        try {
            Assert.assertFalse(action().isElementPresent(page.orderAccepted));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Текст о принятии заказа должен быть скрыт");
        }

        action().click(page.submit);

        page.findModalElement();

        action().click(page.reserveTable);

        try {
            action().waitBy(page.chooseDateAndTimeReserveTableElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы оформления заказа не найден");
        }

    }

    @Test(description = "Забрать еду самостоятельно", priority = 4)
    public void pickYourselfFood(){

        GlobalPage gloPage = new GlobalPage();
        OrderFoodOfRestaurantPage page = new OrderFoodOfRestaurantPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderFood);

        page.findModalElement();

        action().click(page.pickYourself);

        page.findMenuRestaurantElement();
        page.findMenuBarElement();

        action().click(page.barTea);

        page.findBreadcrumbsCartElement();

        try {
            action().waitBy(page.barTeaDropDown);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        action().click(page.barTeaDropDown);

        try {
            action().waitBy(page.openDropDownMenu);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        action().click(page.teaMoreHoney);

        WebElement checkboxHoney = action().findElement(By.xpath("//input[@id='addCardCheck1-0-2']"));

        try {
            Assert.assertTrue(checkboxHoney.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Мед' не отмечен");
        }

        action().click(page.teaMoreLemon);

        WebElement checkboxLemon = action().findElement(By.xpath("//input[@id='addCardCheck1-0-1']"));

        try {
            Assert.assertTrue(checkboxLemon.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Лимон' не отмечен");
        }

        action().click(page.barTeaDropDown);

        try {
            Assert.assertFalse(action().isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Дроп-даун меню не свернулось");
        }

        action().click(page.barCoffee);

        try {
            action().waitBy(page.barCoffeeDropDown);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        action().click(page.barCoffeeDropDown);

        try {
            action().waitBy(page.openDropDownMenu);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        action().click(page.coffeeMoreCoco);

        WebElement checkboxCoco = action().findElement(By.xpath("//input[@id='addCardCheck14-0-2']"));

        try {
            Assert.assertTrue(checkboxCoco.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Кокос' не отмечен");
        }

        action().click(page.coffeeMoreVanilla);

        WebElement checkboxVanilla = action().findElement(By.xpath("//input[@id='addCardCheck14-0-4']"));

        try {
            Assert.assertTrue(checkboxVanilla.isSelected());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Чекбокс с добавкой 'Ваниль' не отмечен");
        }

        action().click(page.barCoffeeDropDown);

        try {
            Assert.assertFalse(action().isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Дроп-даун меню не свернулось");
        }

        action().click(page.barJuice);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        action().click(page.kitchen);

        page.findMenuKitchenElement();

        action().click(page.kitchenSoup);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        action().click(page.kitchenBurger);

        try {
            action().waitBy(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Товар не выбран");
        }

        action().click(page.submitToCart);

        page.findCheckoutPageElement();

        action().click(page.date);
        action().click(page.chooseDateOrTime);

        action().click(page.time);
        action().click(page.chooseDateOrTime);

        action().sendKeys(page.comment, "Заказ еды 'Доставка в апартамент' выполнен автотестом");

        action().click(page.submit);

        page.findOrderAcceptedElement();
        page.findManagerConfirmOrderElement();

        action().click(page.submit);

        page.findModalElement();

        action().click(page.pickYourself);

        page.findMenuRestaurantElement();
        page.findMenuBarElement();

    }

}
