package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.PromosOfPartnersPage;

public class PromosOfPartnersTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Перейти в акцию")
    public void goToPromo(){

        PromosOfPartnersPage page = new PromosOfPartnersPage();
        GlobalPage gloPage = new GlobalPage();

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.promosFromPartners);

        try {
            action().waitBy(page.promosFromPartnersElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Акции от партнеров' не найден");
        }

        action().click(page.promo);

        try {
            action().waitBy(page.promoElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы акции не найден");
        }

        action().click(page.breadcrumbsPromosOfPartners);

        try {
            action().waitBy(page.promosFromPartnersElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Акции от партнеров' не найден");
        }

    }

}
