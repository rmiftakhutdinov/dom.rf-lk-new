package personalAccount.tests;

import org.testng.annotations.*;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;

public class FirefoxTests extends BaseTest {

    @BeforeTest
    public void beforeTest(){

        driver = new DriverManager().getDriver("windows", "firefox");
        browser().maximize();

        driver.get(Configuration.url + "renter");
    }

    @AfterTest
    public void afterTest(){

        driver.close();
    }

    @Test(description = "Раскрыть текст описания апартамента по кнопке 'Подробнее'")
    public void showDescriptionOfApartment() {

    }

}
