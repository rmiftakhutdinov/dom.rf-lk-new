package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.NewsAndNotificationsPage;

public class NewsAndNotificationsTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Уведомление в разделе 'Новости и уведомления'")
    public void goToNotice(){

        NewsAndNotificationsPage page = new NewsAndNotificationsPage();
        GlobalPage gloPage = new GlobalPage();

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.newsAndNotices);

        try {
            action().waitBy(page.newsAndNoticesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Новости и уведомления' не найден");
        }

        String noticeOfList = action().findElement(page.noticeList).getText().replace("…", "").trim();

        action().click(page.noticeList);

        try {
            action().waitBy(page.noticeCard);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент карточки уведомления не найден");
        }

        String noticeOfCard = action().findElement(page.noticeCard).getText();

        try {
            Assert.assertTrue(noticeOfCard.contains(noticeOfList));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Тексты уведомления в списке и в карточке различаются");
        }

        action().click(page.breadcrumbs);

        try {
            action().waitBy(page.newsAndNoticesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Новости и уведомления' не найден");
        }

    }

}
