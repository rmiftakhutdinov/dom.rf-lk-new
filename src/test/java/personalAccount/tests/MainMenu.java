package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.*;
import static java.lang.Thread.sleep;

public class MainMenu extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Главное меню апартамента")
    public void menuOfApartment() throws InterruptedException {

        GlobalPage gloPage = new GlobalPage();
        ProfilePage profPage = new ProfilePage(driver);
        MainMenuPage page = new MainMenuPage(driver);

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(profPage.profile);

        profPage.findProfileElement();

        action().click(page.menu);
        sleep(1000);

        page.findCloseMenu();

        action().click(profPage.profile);
        sleep(1000);

        try {
            Assert.assertFalse(driver.findElement(page.logo).isDisplayed());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент логотипа виден на странице");
        }

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.helpAndContacts);

        try {
            action().waitBy(page.helpAndContactsElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Помощь и контакты' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.promosFromPartners);

        try {
            action().waitBy(page.promosFromPartnersElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Акции от партнеров' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.myOrders);

        try {
            action().waitBy(page.myOrdersElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Мои заказы' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.rentParking);

        try {
            action().waitBy(page.rentParkingElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Аренда машиноместа' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.orderCleaning);

        try {
            action().waitBy(page.orderCleaningElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Заказать уборку' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.orderRepair);

        try {
            action().waitBy(page.orderRepairElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Заказать ремонт' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.orderPassesAndKeys);

        try {
            action().waitBy(page.orderPassesAndKeysElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Пропуска и ключи' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.orderOtherServices);

        try {
            action().waitBy(page.orderOtherServicesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Прочие услуги' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.invoices);

        try {
            action().waitBy(page.invoicesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Счета к оплате' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.paymentHistory);

        try {
            action().waitBy(page.paymentHistoryElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'История платежей' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.consumptionHistory);

        try {
            action().waitBy(page.consumptionHistoryElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'История потребления' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.residencePassport);

        try {
            action().waitBy(page.residencePassportElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Паспорт жилья' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.conditionsAndContract);

        try {
            action().waitBy(page.conditionsAndContractElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Условия и договор' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.newsAndNotices);

        try {
            action().waitBy(page.newsAndNoticesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Новости и уведомления' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.orderFoodFromRestaurant);

        try {
            action().waitBy(page.orderFoodFromRestaurantElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна заказа еды из ресторана не найден");
        }

        action().click(page.closeFoodModal);
        sleep(1000);

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.apartment);

        try {
            action().waitBy(page.apart250);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Апартамент не найден");
        }

        action().click(page.nextObject);

        while (!action().isElementPresent(gloPage.parkingA70)) {

            action().click(gloPage.nextObject);

        }

        action().click(profPage.profile);

        profPage.findProfileElement();
        sleep(1000);

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.parking);

        try {
            action().waitBy(page.parkingA70);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Машиноместо не найдено");
        }

    }

    @Test(description = "Главное меню машиноместа", priority = 1)
    public void menuOfParking() throws InterruptedException {

        GlobalPage gloPage = new GlobalPage();
        ProfilePage profPage = new ProfilePage(driver);
        MainMenuPage page = new MainMenuPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.parkingA70)) {

            action().click(gloPage.nextObject);

        }

        action().click(profPage.profile);

        profPage.findProfileElement();

        action().click(page.menu);
        sleep(1000);

        page.findCloseMenu();

        action().click(profPage.profile);
        sleep(1000);

        try {
            Assert.assertFalse(driver.findElement(page.logo).isDisplayed());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент логотипа виден на странице");
        }

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.helpAndContacts);

        try {
            action().waitBy(page.helpAndContactsElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Помощь и контакты' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.invoices);

        try {
            action().waitBy(page.invoicesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Счета к оплате' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.paymentHistory);

        try {
            action().findElement(page.paymentHistoryElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'История платежей' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.conditionsAndContract);

        try {
            action().waitBy(page.conditionsAndContractElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Условия и договор' не найден");
        }

        page.findSpinner();

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.parking);

        try {
            action().waitBy(page.parkingA70);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Машиноместо не найдено");
        }

        page.findSpinner();

        action().click(page.nextObject);

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(profPage.profile);

        profPage.findProfileElement();
        sleep(1000);

        action().click(page.menu);
        sleep(1000);

        page.logoIsDisplayed();

        action().click(page.apartment);

        try {
            action().waitBy(page.apart250);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Апартамент не найден");
        }

    }

}
