package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.RulesAndContractPage;
import static java.lang.Thread.sleep;

public class RulesAndContractTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Открыть 'Правила проживания' в разделе 'Условия и договор'")
    public void openRulesOfResidence() throws InterruptedException {

        RulesAndContractPage page = new RulesAndContractPage();
        GlobalPage gloPage = new GlobalPage();

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.rulesAndContract);

        try {
            action().waitBy(page.rulesAndContractElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Условия и договор' не найден");
        }

        action().click(page.rulesOfResidence);

        sleep(3000);

        String handleBefore = driver.getWindowHandle();

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String document = "documents/living_rules.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(document));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

    }

    @Test(description = "Открыть галерею договора аренды", priority = 1)
    public void openGalleryInContractOfRent() throws InterruptedException {

        RulesAndContractPage page = new RulesAndContractPage();
        GlobalPage gloPage = new GlobalPage();

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.rulesAndContract);

        try {
            action().waitBy(page.rulesAndContractElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Условия и договор' не найден");
        }

        action().click(page.contractOfRent);

        try {
            action().waitBy(page.contractOfRentElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы галереи не найден");
        }

        action().click(page.img);

        try {
            action().waitBy(page.openJPG);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент открытого фото в режиме 'галерея' не найден");
        }

        String img1 = action().findElement(page.openJPG).getAttribute("src");

        action().click(page.nextImg);

        String img2 = action().findElement(page.openPNG).getAttribute("src");

        try {
            Assert.assertFalse(img1.equals(img2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("id фотографий не различается");
        }

        action().click(page.closeImg);
        action().waitForElementIsInvisibility(page.closeImg);

        String handleBefore = driver.getWindowHandle();

        action().click(page.pdf);

        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String pdf = ".pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(pdf));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Расширение документа не соответствует типу документа");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

        action().click(page.breadcrumbs);

        try {
            action().waitBy(page.rulesAndContractElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Условия и договор' не найден");
        }

    }

}
