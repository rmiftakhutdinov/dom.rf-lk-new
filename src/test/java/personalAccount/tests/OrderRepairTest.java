package personalAccount.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AdminPanelPage;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.OrderRepairPage;
import java.util.Set;
import java.util.stream.Collectors;
import static java.lang.Thread.sleep;

public class OrderRepairTest extends BaseTest {

    private String serviceOrderNumber;
    private String serviceOrderDate;
    private String serviceOrderSum;

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Хлебные крошки в заказе ремонта")
    public void breadcrumbsOrderRepair(){

        GlobalPage gloPage = new GlobalPage();
        OrderRepairPage page = new OrderRepairPage(driver);

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderRepair);

        page.findOrderRepairPageElement();

        // ожидание исчезновения спиннера
        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.sanitaryWorks);

        page.findSanitaryWorksPageElement();

        action().click(page.sanitaryWaterSupply);

        page.findSanitaryWaterSupplyPageElement();

        action().click(page.servicesBreadcrumbs);

        page.findOrderRepairPageElement();

        action().click(page.sanitaryWorks);

        page.findSanitaryWorksPageElement();

        action().click(page.sanitaryWaterSupply);

        page.findSanitaryWaterSupplyPageElement();

        action().click(page.changeToiletBowl);

        try {
            action().waitBy(page.changeToiletBowlElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент услуги 'Замена унитаза' не найден");
        }

        action().click(page.submit);

        page.findRepairServicesCartElement();

        action().sendKeys(page.textAreaComments, "Пожелание");

        action().click(page.submit);

        page.findChooseDateAndTimePageElement();

        action().click(page.orderBreadcrumbs);

        page.findRepairServicesCartElement();

        action().click(page.servicesBreadcrumbs);

        page.findOrderRepairPageElement();

        action().click(page.orderBreadcrumbs);

        String comment = action().findElement(page.textAreaComments).getAttribute("value");

        try {
            Assert.assertEquals(comment, "Пожелание");
        } catch (AssertionError ex){
            action().afterExceptionMessage("Текст комментария не найден");
        }

    }

    @Test(description = "Заказ одной услуги по ремонту", priority = 1)
    public void orderOneRepairService() throws InterruptedException {

        GlobalPage gloPage = new GlobalPage();
        OrderRepairPage page = new OrderRepairPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderRepair);

        page.findOrderRepairPageElement();

        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.sanitaryWorks);

        page.findSanitaryWorksPageElement();

        action().click(page.sanitaryWaterSupply);

        page.findSanitaryWaterSupplyPageElement();

        action().click(page.consultation);

        try {
            action().waitBy(page.consultationElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент карточки услуги не найден");
        }

        action().click(page.submit);

        page.findRepairServicesCartElement();

        action().sendKeys(page.textAreaComments,"Пожелание - заказ одной услуги по ремонту");

        action().click(page.submit);

        page.findChooseDateAndTimePageElement();

        action().click(page.inputDate);

        sleep(100);

        action().click(page.chooseDate);

        action().click(page.inputTime);

        sleep(100);

        action().click(page.chooseTime);

        action().click(page.submit);

        page.findServiceOrderedElement();

        String dateOrder1 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        action().click(page.myOrders);

        try {
            action().waitBy(page.orderedConsultation);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент заказа в списке моих заказов не найден");
        }

        action().click(page.orderedConsultation);

        try {
            action().waitBy(By.xpath("//p[text()='Пожелание - заказ одной услуги по ремонту']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Текст комментария не найден");
        }

        String dateOrder2 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();

        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата и время заказа не совпали");
        }

        String serviceOrderUrl = driver.getCurrentUrl();
        String[] segments = serviceOrderUrl.split("/");

        serviceOrderNumber = segments[segments.length - 1]; // обрезка всей строки до последнего слеша и получение номера заказа

        serviceOrderDate = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText().split("\n")[1];

        serviceOrderSum = action().findElement(By.xpath("//span[@class='ng-binding ng-scope']")).getText();

    }

    @Test(dependsOnMethods = "orderOneRepairService", description = "Проверка создания заказа одной услуги по ремонту в админке", priority = 2)
    public void newOneRepairServiceInAdminPanel() throws InterruptedException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        page.authorizationInAdminPanel();

        action().click(page.dropDownMenuServices);

        try {
            action().waitBy(page.clientServicesSection);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Все заказы клиентов' не найден");
        }

        action().click(page.clientServicesSection);

        page.findServiceOrderNumberInAdminPanel();

        action().click(page.showServicesDetail);

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")), "innerText"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста в детализации заказа не найден");
        }

        sleep(500);

        String serviceOrderNumberAdmin = action().findElement(page.serviceOrderNumber).getText()
                .replace("250-", "").split("\n")[0];
        String serviceOrderNameAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")).getText();
        String serviceOrderSumAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[1]/div[3]")).getText()
                .replace(",00","");
        String serviceOrderDateAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]//div/span")).getText();
        String serviceOrderStatusAdmin = action().findElement(page.serviceOrderStatus).getText();
        String serviceOrderPayStatusAdmin = action().findElement(page.serviceOrderPayStatus).getText();

        try {
            Assert.assertTrue(serviceOrderNumber.contains(serviceOrderNumberAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер заказа в ЛК и в админке не совпал");
        }

        try {
            Assert.assertTrue(serviceOrderDate.contains(serviceOrderDateAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderNameAdmin.contains("Консультация специалиста по водоснабжению"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Название услуги в ЛК и в админке не совпало");
        }

        try {
            Assert.assertTrue(serviceOrderSum.contains(serviceOrderSumAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сумма заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderStatusAdmin.contains("Не выполнен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заказа не верный");
        }

        try {
            Assert.assertTrue(serviceOrderPayStatusAdmin.contains("Бесплатно"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус оплаты заказа не верный");
        }

    }

    @Test(description = "Заказ нескольких услуг по ремонту", priority = 3)
    public void orderFewRepairServices() throws InterruptedException {

        GlobalPage gloPage = new GlobalPage();
        OrderRepairPage page = new OrderRepairPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderRepair);

        page.findOrderRepairPageElement();

        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.sanitaryWorks);

        page.findSanitaryWorksPageElement();

        action().click(page.sanitaryWaterSupply);

        page.findSanitaryWaterSupplyPageElement();

        action().click(page.changeToiletBowl);

        try {
            action().waitBy(page.changeToiletBowlElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент карточки услуги не найден");
        }

        action().click(page.submit);

        page.findRepairServicesCartElement();

        action().click(page.changeToiletBowl);

        try {
            action().waitBy(page.changeToiletBowlElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент карточки услуги не найден");
        }

        action().click(page.submit);

        page.findRepairServicesCartElement();

        action().click(page.addOneMoreService);

        page.findOrderRepairPageElement();

        action().click(page.electricWorks);

        try {
            action().waitBy(page.electricWorksElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Электротехнические работы' не найден");
        }

        action().click(page.repairSwitch);

        try {
            action().waitBy(page.repairSwitchElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент карточки услуги не найден");
        }

        action().click(page.plus);

        sleep(100);

        Assert.assertTrue(action().isElementPresent(By.xpath("//span[contains(text(),'2')]")));

        action().click(page.plus);

        sleep(100);

        Assert.assertTrue(action().isElementPresent(By.xpath("//span[contains(text(),'3')]")));

        action().click(page.minus);

        sleep(100);

        Assert.assertTrue(action().isElementPresent(By.xpath("//span[contains(text(),'2')]")));

        action().click(page.submit);

        page.findRepairServicesCartElement();

        action().click(page.addOneMoreService);

        page.findOrderRepairPageElement();

        action().click(page.carpentryWorks);

        try {
            action().waitBy(page.carpentryWorksElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Плотницкие работы' не найден");
        }

        action().click(page.fittingPeephole);

        try {
            action().waitBy(page.fittingPeepholeElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент карточки услуги не найден");
        }

        action().click(page.submit);

        page.findRepairServicesCartElement();

        action().click(page.deleteRepairService);

        try {
            action().waitForElementIsInvisibility(page.changeToiletBowl);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент услуги не пропал из корзины");
        }

        action().sendKeys(page.textAreaComments, "Пожелание - заказ нескольких услуг по ремонту");

        action().click(page.submit);

        page.findChooseDateAndTimePageElement();

        action().click(page.inputDate);

        sleep(100);

        action().click(page.chooseDate);

        action().click(page.inputTime);

        sleep(100);

        action().click(page.chooseTime);

        action().click(page.submit);

        page.findServiceOrderedElement();

        String dateOrder1 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        action().click(page.myOrders);

        try {
            action().waitBy(page.orderedRepair);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент заказа в списке моих заказов не найден");
        }

        action().click(page.orderedRepair);

        try {
            action().waitBy(By.xpath("//p[text()='Пожелание - заказ нескольких услуг по ремонту']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Текст комментария не найден");
        }

        String dateOrder2 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();

        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата и время заказа не совпали");
        }

        String serviceOrderUrl = driver.getCurrentUrl();
        String[] segments = serviceOrderUrl.split("/");

        serviceOrderNumber = segments[segments.length - 1]; // обрезка всей строки до последнего слеша и получение номера заказа

        serviceOrderDate = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText().split("\n")[1];

        serviceOrderSum = action().findElement(By.xpath("//span[@class='ng-binding ng-scope']")).getText();

    }

    @Test(dependsOnMethods = "orderFewRepairServices", description = "Проверка создания заказа нескольких услуг по ремонту в админке", priority = 4)
    public void newFewRepairServicesInAdminPanel() throws InterruptedException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        if (!action().elementIsNotPresent(page.userName)){

            page.authorizationInAdminPanel();

        }

        action().click(page.dropDownMenuServices);

        try {
            action().waitBy(page.clientServicesSection);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Все заказы клиентов' не найден");
        }

        action().click(page.clientServicesSection);

        page.findServiceOrderNumberInAdminPanel();

        action().click(page.showServicesDetail);

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")), "innerText"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста в детализации заказа не найден");
        }

        sleep(500);

        String serviceOrderNumberAdmin = action().findElement(page.serviceOrderNumber).getText()
                .replace("250-", "").split("\n")[0];
        String serviceOrderSumAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[1]/div[3]")).getText()
                .replace(",00","");
        String serviceOrderDateAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]//div/span")).getText();
        String serviceOrderStatusAdmin = action().findElement(page.serviceOrderStatus).getText();
        String serviceOrderPayStatusAdmin = action().findElement(page.serviceOrderPayStatus).getText();

        Set<String> nameServices = driver.findElements(By.xpath("//tbody/tr[1]/td[2]/div/small[1]"))
                .stream().map(element -> element.getText().trim()).collect(Collectors.toSet());

        for (String name : nameServices){

            if (name.contains("Врезка дверного глазка")){

                break;

            }

        }

        try {
            Assert.assertTrue(serviceOrderNumber.contains(serviceOrderNumberAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер заказа в ЛК и в админке не совпал");
        }

        try {
            Assert.assertTrue(serviceOrderDate.contains(serviceOrderDateAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderSum.contains(serviceOrderSumAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сумма заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderStatusAdmin.contains("Не выполнен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заказа не верный");
        }

        try {
            Assert.assertTrue(serviceOrderPayStatusAdmin.contains("Не оплачен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус оплаты заказа не верный");
        }

    }

}
