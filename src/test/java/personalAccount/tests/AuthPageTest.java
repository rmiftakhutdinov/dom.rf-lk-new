package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.*;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.data.CommonData;
import personalAccount.pageObjects.AuthorizationPage;
import static java.lang.Thread.sleep;

public class AuthPageTest extends BaseTest {

    @Test(description = "Переход на главную страницу сайта")
    public void goToSiteHomePage() {

        AuthorizationPage page = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        page.findAuthPageElement();

        try {
            action().waitBy(page.logo);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент логотипа ДОМ.РФ не найден");
        }

        action().click(page.logo);

        try {
            action().waitBy(page.homePageElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Главная страница не загрузилась");
        }

    }

    @Test(description = "Открыть документ 'Условия использования'", priority = 1)
    public void openTermsOfUse() throws InterruptedException {

        AuthorizationPage page = new AuthorizationPage(driver);
        String handleBefore = driver.getWindowHandle();

        driver.get(Configuration.url + "renter");

        page.findAuthPageElement();

        action().click(page.termsOfUse);

        sleep(3000);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String document = "documents/terms_of_service.pdf";

        try {
            Assert.assertTrue(driver.getCurrentUrl().contains(document));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Документ по указанному адресу не найден");
        }

        driver.close();
        driver.switchTo().window(handleBefore);

    }

    @Test(description = "Отправка ссылки на эл.почту для восстановления пароля", priority = 2)
    public void changePasswordByEmail() {

        AuthorizationPage page = new AuthorizationPage(driver);
        CommonData user = new CommonData();

        driver.get(Configuration.url + "renter");

        page.findAuthPageElement();

        action().click(page.changePassword);

        try {
            action().waitBy(page.backToAuthPage);
        } catch (TimeoutException ex) {
            action().afterExceptionMessage("Элемент страницы ввода почты и телефона не найден");
        }

        action().sendKeys(page.input, user.userEmail);

        action().click(page.nextPage);

        try {
            action().waitBy(page.sendLinkAgain);
        } catch (TimeoutException ex) {
            action().afterExceptionMessage("Элемент страницы повторной отправки ссылки на эл.почту не найден");
        }

        action().click(page.sendLinkAgain);

        try {
            action().waitBy(page.spinner);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент спиннера не найден");
        }

        try {
            Assert.assertTrue(action().findElement(page.sendLinkAgain).isDisplayed());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент страницы повторной отправки ссылки на эл.почту не найден");
        }

    }

    @Test(description = "Отправка кода на номер телефона для восстановления пароля", priority = 3)
    public void changePasswordByPhoneNumber() {

        AuthorizationPage page = new AuthorizationPage(driver);
        CommonData user = new CommonData();

        driver.get(Configuration.url + "renter");

        page.findAuthPageElement();

        action().click(page.changePassword);

        try {
            action().waitBy(page.backToAuthPage);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы ввода почты и телефона не найден");
        }

        action().sendKeys(page.input, user.userPhoneCab);

        action().click(page.nextPage);

        try {
            action().waitBy(page.sendCodPage);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы ввода кода не найден");
        }

        try {
            action().waitBy(page.changePhoneNumber, 65);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки изменения номера телефона не найден");
        }

        action().click(page.changePhoneNumber);

        try {
            action().waitBy(page.backToAuthPage);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы ввода почты и телефона не найден");
        }

        action().click(page.nextPage);

        try {
            action().waitBy(page.textRequestCodAgain);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Подсказка о повторном запросе пароля не появилась");
        }

        try {
            action().waitBy(page.sendCodAgain, 65);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент кнопки повторного запроса кода не найден");
        }

        action().click(page.sendCodAgain);

        try {
            action().waitBy(page.textRequestCodAgain);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Подсказка о повторном запросе пароля не появилась");
        }

    }

    @Test(description = "Авторизация по номеру телефона", priority = 4)
    public void authorizationByPhone() {

        AuthorizationPage page = new AuthorizationPage(driver);
        CommonData user = new CommonData();

        driver.get(Configuration.url + "renter");

        page.findAuthPageElement();

        action().sendKeys(page.userName, user.userPhoneCab);
        action().sendKeys(page.userPassword, user.userPass);

        try {
            Assert.assertTrue(action().findElement(page.userPassword).getAttribute("type").equals("password"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Пароль не скрыт звездочками");
        }

        action().click(page.eye);

        try {
            Assert.assertTrue(action().findElement(page.userPassword).getAttribute("type").equals("text"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Пароль не раскрыт");
        }

        action().click(page.submit);

        page.findCabinetElement();

    }

}
