package personalAccount.tests;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.RentObjectAdminPanelPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import static java.lang.Thread.sleep;

public class RentObjectAdminTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        driver.get(Configuration.url + "admin");
        AuthorizationPage auth = new AuthorizationPage(driver);

        auth.authorizationInAdminPanel();

    }

    @Test(description = "Выселение/заселение в апартамент №250")
    public void rentApartment() throws InterruptedException, AWTException {

        RentObjectAdminPanelPage page = new RentObjectAdminPanelPage(driver);

        action().sendKeys(page.filterInput, page.apartmentNumber250 + Keys.ENTER);

        try{
            action().waitBy(page.apart250, 30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент апартамента в списке не найден");
        }

        action().click(page.apart250);

        try{
            action().waitBy(page.contracts);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент карточки апартамента не найден");
        }


        if (action().elementIsNotPresent(page.labelStatusFreeApartment)){

            action().click(page.contracts);

            // Клик по дроп-дауну действующего договора
            action().click(By.xpath("//tbody//span[contains(text(), 'Действующий')]/ancestor::tr/td[4]/div"));

            try {
                action().waitBy(page.finishContract);
            } catch (TimeoutException ex){
                action().afterExceptionMessage("Элемент кнопки завершения договора не найден");
            }

            action().click(page.finishContract);

            sleep(1000);

            if(action().findElement(page.finishContractConfirm).isDisplayed()){

                action().click(page.leaveAccess);

                try {
                    Assert.assertFalse(action().findElement(page.leaveAccess).isSelected());
                } catch (AssertionError ex){
                    action().afterExceptionMessage("Отметка в чекбоксе об оставлении доступа арендатору не снята");
                }

                action().click(page.finishContractConfirm);

            } else {

                try {
                    action().waitBy(page.settleModal);
                } catch (TimeoutException ex){
                    action().afterExceptionMessage("Элемент модально окна завершения договора не найден");
                }

                action().click(page.checkboxCall);
                action().click(page.leaveAccessUS);

                try {
                    Assert.assertFalse(action().findElement(page.leaveAccess).isSelected());
                } catch (AssertionError ex){
                    action().afterExceptionMessage("Отметка в чекбоксе об оставлении доступа арендатору не снята");
                }

                try {
                    Assert.assertTrue(driver.findElement(page.checkboxCall).isSelected());
                } catch (AssertionError ex){
                    action().afterExceptionMessage("Отметка в чекбоксе о звонке в УК не проставлена");
                }

                action().click(page.ejectConfirm);

            }

            try {
                action().waitBy(page.changeStatus);
            } catch (TimeoutException ex){
                action().afterExceptionMessage("Элемент кнопки изменения статуса апартамента не найден");
            }

        }

        try {
            action().waitBy(page.labelStatusFreeApartment);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Статус апартамента 'Свободен' не найден");
        }

        action().click(page.changeStatus);

        try {
            action().waitBy(page.statusRented);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна изменения статуса не найден");
        }

        action().click(page.statusRented);
        action().click(page.save);

        try {
            action().waitBy(page.settlementElement, 30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы заселения не найден");
        }

        try {
            action().waitForElementIsInvisibility(By.xpath("//div[@class='col-sm-8 ng-scope']/span/i"),30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Спиннер не исчез");
        }

        for (;;){
            if (action().elementIsNotPresent(page.inputIndication)){
                driver.navigate().refresh();
            } else {
                break;
            }
        }

        action().click(page.dropDownRenter);

        action().sendKeys(page.inputChoiseRenter, page.renterID + Keys.ENTER);

        try {
            Assert.assertTrue(action().findElement(page.renterInput202).isDisplayed());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент выбранного арендатора не найден");
        }

        action().click(page.calendar);
        action().click(page.nextMonth);
        action().click(page.setDate);

        try {
            action().waitForElementIsInvisibility(By.xpath("//div[@id='schedule__overlay']/i"),10);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Спиннер не исчез");
        }

        try {
            action().waitBy(page.inputIndication);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Инпуты ввода показаний счетчиков не найдены");
        }

        action().click(page.inputIndication);

        action().click(page.indication);
        sleep(500);

        action().click(page.indication);
        sleep(500);

        action().click(page.indication);
        sleep(500);

        action().click(page.indication);
        sleep(500);

        action().click(page.indication);
        sleep(500);

        String jpg = System.getProperty("user.dir") + "\\files\\111.jpg";
        action().setClipboardData(jpg);
        action().click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(500);

        String pdf = System.getProperty("user.dir") + "\\files\\222.pdf";
        action().setClipboardData(pdf);
        action().click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(500);

        String png = System.getProperty("user.dir") + "\\files\\333.png";
        action().setClipboardData(png);
        action().click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        try {
            action().waitForElementIsInvisibility(By.xpath("//div[contains(text(),'Дождитесь загрузки файлов')]"),10);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Кнопка сабмита не стала активной");
        }

        sleep(1000);

        action().click(page.nextStep);

        try {
            action().waitBy(page.nextStepApartmentElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы подтверждения заселения не найден");
        }

        List<WebElement> images=driver.findElements(By.xpath("//div/img"));

        try {
            Assert.assertTrue(images.size() == 2);
        } catch (AssertionError ex){
            action().afterExceptionMessage("Неверное количество загруженных картинок");
        }

        try {
            Assert.assertTrue(action().findElement(By.xpath("//span[@title='PDF']")).isDisplayed());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент загруженного файла PDF не найден");
        }

        action().click(page.settle);

        try {
            action().waitBy(page.settleModalConfirm);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна подтверждения заселения не найден");
        }

        action().click(page.settleConfirm);

        try {
            action().waitBy(page.labelStatusRentedApartment);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Статус апартамента 'Арендован' не найден");
        }

        action().click(page.contracts);

        try {
            new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Действующий')]")));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Статус договора 'Действующий' не найден");
        }

    }

    @Test(description = "Приём/сдача машиноместа №А70", priority = 1)
    public void rentParking() throws InterruptedException {

        RentObjectAdminPanelPage page = new RentObjectAdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(page.filter));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент строки поиска в списке апартаментов не найден");
        }

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(page.objectNumber));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент апартамента в списке апартаментов не найден");
        }

        action().selectByValue(page.changeObjectType, "string:parkings");

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(page.filter));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент строки поиска в списке машиномест не найден");
        }

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(page.objectNumber));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент машиноместа в списке апартаментов не найден");
        }

        action().sendKeys(page.filterInput, page.parkingNumberA70 + Keys.ENTER);

        try{
            action().waitBy(page.parkingA70, 30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент машиноместа в списке не найден");
        }

        action().click(page.parkingA70);

        try{
            action().waitBy(page.contracts);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент карточки машиноместа не найден");
        }

        if (action().elementIsNotPresent(page.labelStatusFreeParking)){

            action().click(page.contracts);

            // Клик по дроп-дауну действующего договора
            action().click(By.xpath("//tbody//span[contains(text(), 'Действующий')]/ancestor::tr/td[4]/div"));

            try {
                action().waitBy(page.finishContract);
            } catch (TimeoutException ex){
                action().afterExceptionMessage("Элемент кнопки завершения договора не найден");
            }

            action().click(page.finishContract);

            sleep(1000);

            try {
                action().waitBy(page.finishContractConfirm);
            } catch (TimeoutException ex){
                action().afterExceptionMessage("Элемент модального окна завершения договора не найден");
            }

            action().click(page.leaveAccess);

            try {
                Assert.assertFalse(action().findElement(page.leaveAccess).isSelected());
            } catch (AssertionError ex){
                action().afterExceptionMessage("Отметка в чекбоксе об оставлении доступа арендатору не снята");
            }

            action().click(page.finishContractConfirm);

            try {
                action().waitBy(page.changeStatus);
            } catch (TimeoutException ex){
                action().afterExceptionMessage("Элемент кнопки изменения статуса апартамента не найден");
            }

        }

        try {
            action().waitBy(page.labelStatusFreeParking);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Статус машиноместа 'Свободен' не найден");
        }

        action().click(page.changeStatus);

        try {
            action().waitBy(page.statusRented);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна изменения статуса не найден");
        }

        action().click(page.statusRented);
        action().click(page.save);

        try {
            action().waitBy(page.leaseElement, 30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы сдачи машиноместа не найден");
        }

        try {
            action().waitForElementIsInvisibility(By.xpath("//div[@class='col-sm-8 ng-scope']/span/i"),30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Спиннер не исчез");
        }

        action().click(page.dropDownRenter);

        action().sendKeys(page.inputChoiseRenter, page.renterID + Keys.ENTER);

        try {
            Assert.assertTrue(action().findElement(page.renterInput202).isDisplayed());
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент выбранного арендатора не найден");
        }

        try {
            action().waitForElementIsInvisibility(By.xpath("//div[@id='schedule__overlay']/i"),10);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Спиннер не исчез");
        }

        action().click(page.nextStep);

        try {
            action().waitBy(page.nextStepParkingElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы подтверждения сдачи машиноместа не найден");
        }

        action().click(page.lease);

        try {
            action().waitBy(page.rentModalConfirm);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент модального окна подтверждения сдачи машиноместа не найден");
        }

        action().click(page.rentConfirm);

        try {
            action().waitBy(page.labelStatusRentedParking);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Статус машиноместа 'Арендован' не найден");
        }

        action().click(page.contracts);

        try {
            new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Действующий')]")));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Статус договора 'Действующий' не найден");
        }
    }

}
