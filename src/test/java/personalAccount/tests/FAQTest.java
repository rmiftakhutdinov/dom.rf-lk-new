package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.FAQPage;
import static java.lang.Thread.sleep;

public class FAQTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Свернуть и развернуть статьи в разделе 'Помощь и контакты'")
    public void hideAndShowItems() throws InterruptedException {

        FAQPage page = new FAQPage();

        action().click(page.FAQ);

        try {
            action().waitBy(page.FAQElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы 'Помощь и контакты' не найден");
        }

        sleep(500);

        action().click(page.parking);

        sleep(500);

        try {
            Assert.assertTrue(action().findElement(page.parkingElement).getText().contains("Для арендных апартаментов"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статья в разделе 'Общие' не развернулась");
        }

        action().click(page.parking);

        sleep(500);

        try {
            Assert.assertFalse(action().findElement(page.parkingElement).isDisplayed());
        } catch (AssertionError ex) {
            action().afterExceptionMessage("Статья в разделе 'Общие' не свернулась");
        }

        action().click(page.apartments);

        sleep(500);

        try {
            Assert.assertTrue(action().findElement(page.internet).isDisplayed());
        } catch (AssertionError ex) {
            action().afterExceptionMessage("Раздел 'Апартаменты' не развернулся");
        }

        action().click(page.internet);

        sleep(500);

        try {
            Assert.assertTrue(action().findElement(page.internetElement).getText().contains("Во всех апартаментах проведен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статья в разделе 'Апартаменты' не развернулась");
        }

        action().click(page.internet);

        sleep(500);

        try {
            Assert.assertFalse(action().findElement(page.parkingElement).isDisplayed());
        } catch (AssertionError ex) {
            action().afterExceptionMessage("Статья в разделе 'Апартаменты' не свернулась");
        }

        action().click(page.apartments);

        sleep(500);

        try {
            Assert.assertFalse(action().findElement(page.internet).isDisplayed());
        } catch (AssertionError ex) {
            action().afterExceptionMessage("Раздел 'Апартаменты' не свернулся");
        }

    }

}
