package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.ProfilePage;

import static java.lang.Thread.sleep;

public class ProfileTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Смена эл.почты и телефона")
    public void changeEmailAndPhoneNumber() throws InterruptedException {

        ProfilePage page = new ProfilePage(driver);

        action().click(page.profile);

        page.findProfileElement();

        action().sendKeys(page.email, "test@test.ru");
        action().sendKeys(page.phone, "8888888888");

        sleep(1000);

        action().click(page.save);

        try {
            action().waitForElementIsInvisibility(page.profileElement, 30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы профиля пользователя не исчез");
        }

        action().click(page.profile);

        try {
            action().waitBy(page.profileElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы профиля пользователя не найден");
        }

        try {
            Assert.assertTrue(action().findElement(page.email).getAttribute("value").equals("test@test.ru"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Адрес электронной почты не был изменен");
        }

        try {
            Assert.assertTrue(action().findElement(page.phone).getAttribute("value").equals("+7 888 888-88-88"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер телефона не был изменен");
        }

        action().sendKeys(page.email, "rm@cometrica.ru");
        action().sendKeys(page.phone, " 927 240 11 45");

        action().click(page.save);

        try {
            action().waitForElementIsInvisibility(page.profileElement, 30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы профиля пользователя не исчез");
        }

        action().click(page.profile);

        try {
            action().waitBy(page.profileElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы профиля пользователя не найден");
        }

        try {
            Assert.assertTrue(action().findElement(page.email).getAttribute("value").equals("rm@cometrica.ru"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Адрес электронной почты не был изменен");
        }

        try {
            Assert.assertTrue(action().findElement(page.phone).getAttribute("value").equals("+7 927 240-11-45"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер телефона не был изменен");
        }

    }

    @Test(description = "Смена пароля", priority = 1)
    public void changePassword(){

        ProfilePage page = new ProfilePage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findCabinetElement();

        action().click(page.profile);

        page.findProfileElement();

        action().sendKeys(page.oldPass, "test2015");

        try {
            Assert.assertTrue(action().findElement(page.oldPass).getAttribute("type").equals("password"));
        }catch (AssertionError ex){
            action().afterExceptionMessage("Пароль не скрыт звездочками");
        }

        action().click(page.eyeOld);

        try {
            Assert.assertTrue(action().findElement(page.oldPass).getAttribute("type").equals("text"));
        }catch (AssertionError ex){
            action().afterExceptionMessage("Пароль не отобразился");
        }

        action().sendKeys(page.newPass, "666666");

        try {
            Assert.assertTrue(action().findElement(page.newPass).getAttribute("type").equals("password"));
        }catch (AssertionError ex){
            action().afterExceptionMessage("Пароль не скрыт звездочками");
        }

        action().click(page.eyeNew);

        try {
            Assert.assertTrue(action().findElement(page.newPass).getAttribute("type").equals("text"));
        }catch (AssertionError ex){
            action().afterExceptionMessage("Пароль не отобразился");
        }

        action().click(page.save);

        try {
            action().waitForElementIsInvisibility(page.profileElement, 30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы профиля пользователя не исчез");
        }

        action().click(page.profile);

        page.findProfileElement();

        action().sendKeys(page.oldPass, "666666");
        action().sendKeys(page.newPass, "test2015");

        action().click(page.save);

        try {
            action().waitForElementIsInvisibility(page.profileElement, 30);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы профиля пользователя не исчез");
        }

        auth.findCabinetElement();

    }

    @Test(description = "Смена языка", priority = 2)
    public void changeLanguage(){

        ProfilePage page = new ProfilePage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findCabinetElement();

        action().click(page.profile);

        page.findProfileElement();

        action().click(page.changeToEnglish);

        try {
            action().waitBy(page.engElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент перевода текста на английский язык не найден");
        }

        action().click(page.changeToRussian);

        try {
            action().waitBy(page.rusElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент перевода текста на русский язык не найден");
        }

    }

    @Test(description = "Выход из авторизации", priority = 3)
    public void logout(){

        ProfilePage page = new ProfilePage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findCabinetElement();

        action().click(page.profile);

        page.findProfileElement();

        action().click(page.logout);

        auth.findAuthPageElement();

    }

}
