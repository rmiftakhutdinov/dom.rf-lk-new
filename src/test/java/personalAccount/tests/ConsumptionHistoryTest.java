package personalAccount.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.ConsumptionHistoryPage;
import personalAccount.pageObjects.GlobalPage;

public class ConsumptionHistoryTest extends BaseTest {

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorizationSecond();
    }

    @Test(description = "Просмотр истории потребления")
    public void viewConsumptionHistory(){

        ConsumptionHistoryPage page = new ConsumptionHistoryPage();
        GlobalPage gloPage = new GlobalPage();
        Actions action = new Actions(driver);

        while (!action().isElementPresent(gloPage.apart221)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.consumptionHistory);

        try {
            action().waitBy(page.consumptionHistoryElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'История потребления' не найден");
        }

        action().click(page.coldWater);

        WebElement coldWaterElement = action().findElement(By.xpath("//div[1]/div[@class='bar cold_water']"));
        action.moveToElement(coldWaterElement).perform();

        String coldWaterMonthOfGraph = action().findElement(page.monthDataOfGraph).getText();
        String coldWaterMetersOfGraph = action().findElement(page.metersDataOfGraph).getText();

        String coldWaterMonth = action().findElement(page.monthData).getText();
        String coldWaterMeters = action().findElement(page.metersData).getText();

        try {
            Assert.assertTrue(coldWaterMonthOfGraph.contains(coldWaterMonth));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Значения в графике и в списке различаются");
        }

        try {
            Assert.assertTrue(coldWaterMetersOfGraph.contains(coldWaterMeters));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Значения в графике и в списке различаются");
        }

        action().click(page.hotWater);

        WebElement elementHotWater = action().findElement(By.xpath("//div[1]/div[@class='bar hot_water']"));
        action.moveToElement(elementHotWater).perform();

        String hotWaterMonthOfGraph = action().findElement(page.monthDataOfGraph).getText();
        String hotWaterMetersOfGraph = action().findElement(page.metersDataOfGraph).getText();

        String hotWaterMonth = action().findElement(page.monthData).getText();
        String hotWaterMeters = action().findElement(page.metersData).getText();

        try {
            Assert.assertTrue(hotWaterMonthOfGraph.contains(hotWaterMonth));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Значения в графике и в списке различаются");
        }

        try {
            Assert.assertTrue(hotWaterMetersOfGraph.contains(hotWaterMeters));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Значения в графике и в списке различаются");
        }

        action().click(page.electric);

        WebElement elementElectric = action().findElement(By.xpath("//div[1]/div[@class='bar electricity']"));
        action.moveToElement(elementElectric).perform();

        String electricMonthOfGraph = action().findElement(page.monthDataOfGraph).getText();
        String electricMetersOfGraph = action().findElement(page.metersDataOfGraph).getText();

        String electricMonth = action().findElement(page.monthData).getText();
        String electricMeters = action().findElement(page.metersData).getText();

        try {
            Assert.assertTrue(electricMonthOfGraph.contains(electricMonth));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Значения в графике и в списке различаются");
        }

        try {
            Assert.assertTrue(electricMetersOfGraph.contains(electricMeters));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Значения в графике и в списке различаются");
        }

    }

}
