package personalAccount.tests;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.*;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.ObjectsNoDataPage;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.ProfilePage;
import static java.lang.Thread.sleep;

public class ObjectsNoDataTest extends BaseTest {

    @BeforeTest
    public void beforeTest(){
        System.out.println("BeforeTest method is starting!");
        System.out.println();
    }

    @BeforeMethod
    public void beforeMethod(){

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser); // webDriver initialization
        browser().maximize();

        driver.get(Configuration.url + "renter");
    }

    @AfterMethod
    public void afterMethod(){

        if (Configuration.closeDriverAfterTests){
            if(driver != null){
                driver.close();
            }
        }
    }

    @AfterTest
    public void afterTest(){
        System.out.println("AfterTest method is starting!");
    }

    @Test(description = "Арендатор без контрактов")
    public void noContracts() throws InterruptedException {

        ObjectsNoDataPage page = new ObjectsNoDataPage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);

        auth.findAuthPageElement();

        auth.authorizationThird();

        try {
            action().waitBy(page.noApartmentElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент личного кабинета без контрактов не найден");
        }

    }

    @Test(description = "Переход в разделы заказа услуг из раздела 'Мои заказы'", priority = 1)
    public void goToOrderServices() throws InterruptedException {

        ObjectsNoDataPage page = new ObjectsNoDataPage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);
        GlobalPage gloPage = new GlobalPage();

        auth.findAuthPageElement();

        auth.authorizationSecond();

        while (!action().isElementPresent(gloPage.apart222)) {

            action().click(page.nextObject);

        }

        action().click(page.myOrders);

        try {
            action().waitBy(page.myOrdersElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Мои заказы' не найден");
        }

        action().click(page.orderCleaning);

        try {
            action().waitBy(page.cleaningElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Заказать уборку' не найден");
        }

        driver.navigate().back();
        page.findMyOrdersPageElement();

        action().click(page.orderRepair);

        try {
            action().waitBy(page.repairElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Заказать ремонт' не найден");
        }

        driver.navigate().back();
        page.findMyOrdersPageElement();

        action().click(page.orderPassesAndKeys);

        try {
            action().waitBy(page.passesAndKeysElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Пропуска и ключи' не найден");
        }

        driver.navigate().back();
        page.findMyOrdersPageElement();

        action().click(page.orderOtherServices);

        try {
            action().waitBy(page.otherServicesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Прочие услуги' не найден");
        }

        driver.navigate().back();
        page.findMyOrdersPageElement();

    }

    @Test(description = "Переключение вкладок в разделе 'История платежей' в апартаменте", priority = 2)
    public void apartmentPaymentHistory() throws InterruptedException {

        ObjectsNoDataPage page = new ObjectsNoDataPage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);
        GlobalPage gloPage = new GlobalPage();

        auth.findAuthPageElement();

        auth.authorizationSecond();

        while (!action().isElementPresent(gloPage.apart222)) {

            action().click(page.nextObject);

        }

        action().click(page.paymentHistory);

        try {
            action().waitBy(page.paymentHistoryElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'История платежей' не найден");
        }

        action().click(page.rent);

        try {
            action().waitBy(page.rentElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Аренда' не найден");
        }

        action().click(page.communal);

        try {
            action().waitBy(page.communalElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Коммунальные' не найден");
        }

        action().click(page.services);

        try {
            action().waitBy(page.servicesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Услуги' не найден");
        }

        action().click(page.other);

        try {
            action().waitBy(page.otherElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Прочие' не найден");
        }

        action().click(page.allPayments);

        try {
            action().waitBy(page.allPaymentsElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Все' не найден");
        }

    }

    @Test(description = "Переключение вкладок в разделе 'История платежей' в машиноместе", priority = 3)
    public void parkingPaymentHistory() throws InterruptedException {

        ObjectsNoDataPage page = new ObjectsNoDataPage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);

        auth.findAuthPageElement();

        auth.authorizationSecond();

        while (!action().isElementPresent(page.parkingA71)) {

            action().click(page.nextObject);

        }

        action().click(page.paymentHistory);

        try {
            action().waitBy(page.paymentHistoryElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'История платежей' не найден");
        }

        action().click(page.rent);

        try {
            action().waitBy(page.rentElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Аренда' не найден");
        }

        action().click(page.services);

        try {
            action().waitBy(page.servicesElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Услуги' не найден");
        }

        action().click(page.other);

        try {
            action().waitBy(page.otherElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Прочие' не найден");
        }

        action().click(page.allPayments);

        try {
            action().waitBy(page.allPaymentsElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент вкладки 'Все' не найден");
        }

    }

    @Test(description = "Переключение вкладок в разделе 'История потребления'", priority = 4)
    public void noConsumptionHistory() throws InterruptedException {

        ObjectsNoDataPage page = new ObjectsNoDataPage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);
        GlobalPage gloPage = new GlobalPage();

        auth.findAuthPageElement();

        auth.authorizationSecond();

        while (!action().isElementPresent(gloPage.apart222)) {

            action().click(page.nextObject);

        }

        action().click(page.consumptionHistory);

        try {
            action().waitBy(page.consumptionHistoryElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'История потребления' не найден");
        }

        action().click(page.coldWater);

        page.findNoConsumptionHistoryElement();

        try {
            Assert.assertTrue(action().findElement(page.coldWater).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент активной вкладки 'Холодная вода' не найден");
        }

        action().click(page.hotWater);

        page.findNoConsumptionHistoryElement();

        try {
            Assert.assertTrue(action().findElement(page.hotWater).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент активной вкладки 'Горячая вода' не найден");
        }

        action().click(page.electric);

        page.findNoConsumptionHistoryElement();

        try {
            Assert.assertTrue(action().findElement(page.electric).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент активной вкладки 'Электричество' не найден");
        }

    }

    @Test(description = "Нет счетов для оплаты", priority = 5)
    public void noPayments() throws InterruptedException {

        ObjectsNoDataPage page = new ObjectsNoDataPage(driver);
        AuthorizationPage auth = new AuthorizationPage(driver);
        ProfilePage profPage = new ProfilePage(driver);
        GlobalPage gloPage = new GlobalPage();

        auth.findAuthPageElement();

        auth.authorizationSecond();

        while (!action().isElementPresent(gloPage.apart222)) {

            action().click(page.nextObject);

        }

        action().click(profPage.profile);

        profPage.findProfileElement();

        action().click(page.menu);
        sleep(1000);

        action().click(page.payments);

        try {
            action().waitBy(page.noPaymentsElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста об отсутствии счетов не найден");
        }

        try {
            Assert.assertTrue(action().findElement(page.terminalRoscap).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент активной вкладки 'Аренда' не найден");
        }

        action().click(page.terminalPayture);

        try {
            action().waitBy(page.noPaymentsElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста об отсутствии счетов не найден");
        }

        try {
            Assert.assertTrue(action().findElement(page.terminalPayture).getAttribute("class").contains("active"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент активной вкладки 'Аренда' не найден");
        }

    }

}
