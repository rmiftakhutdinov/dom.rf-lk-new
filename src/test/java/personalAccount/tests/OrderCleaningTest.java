package personalAccount.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AdminPanelPage;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.OrderCleaningPage;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import static java.lang.Thread.sleep;

public class OrderCleaningTest extends BaseTest {

    private String serviceOrderNumber;
    private String serviceOrderDate;
    private String serviceOrderSum;

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Хлебные крошки в заказе уборки")
    public void breadcrumbsOrderCleaning(){

        GlobalPage gloPage = new GlobalPage();
        OrderCleaningPage page = new OrderCleaningPage(driver);

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderCleaning);

        page.findOrderCleaningPageElement();

        // ожидание исчезновения спиннера
        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.generalCleaning);

        page.findGeneralCleaningElement();

        action().click(page.cleaningDetailShow);

        try {
            action().waitBy(page.generalDetailInfo);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент детального описания генеральной уборки не найден");
        }

        action().click(page.cleaningDetailClose);

        try {
            Assert.assertFalse(action().isElementPresent(page.generalDetailInfo));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент детального описания генеральной уборки не скрылся");
        }

        action().click(page.servicesBreadcrumbs);

        page.findOrderCleaningPageElement();

        action().click(page.maintainingCleaning);

        page.findMaintainingCleaningElement();

        action().click(page.cleaningDetailShow);

        try {
            action().waitBy(page.maintainingDetailInfo);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент детального описания поддерживающей уборки не найден");
        }

        try {
            Assert.assertFalse(action().isElementPresent(page.generalDetailInfo));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Найден элемент детального описания генеральной уборки");
        }

        action().click(page.cleaningDetailClose);

        try {
            Assert.assertFalse(action().isElementPresent(page.maintainingDetailInfo));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Элемент детального описания поддерживающей уборки не скрылся");
        }

        action().click(page.servicesBreadcrumbs);

        page.findOrderCleaningPageElement();

        action().click(page.generalCleaning);

        page.findGeneralCleaningElement();

        action().sendKeys(page.textAreaComments, "Пожелание");

        action().click(page.servicesBreadcrumbs);

        page.findOrderCleaningPageElement();

        action().click(page.orderBreadcrumbs);

        String comment = action().findElement(By.xpath("//textarea")).getAttribute("value");

        try {
            Assert.assertEquals(comment,"Пожелание");
        } catch (AssertionError ex){
            action().afterExceptionMessage("Текст введенного комментария не найден");
        }

    }

    @Test(description = "Заказ одной услуги по уборке", priority = 1)
    public void orderOneCleaningService() throws InterruptedException {

        GlobalPage gloPage = new GlobalPage();
        OrderCleaningPage page = new OrderCleaningPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderCleaning);

        page.findOrderCleaningPageElement();

        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.generalCleaning);

        page.findGeneralCleaningElement();

        action().click(page.inputDate);

        sleep(100);

        action().click(page.chooseDate);

        action().click(page.inputTime);

        sleep(100);

        action().click(page.chooseTime);

        action().sendKeys(page.textAreaComments, "Пожелание - заказ одной услуги по уборке");

        action().click(page.submit);

        page.findServiceOrderedElement();

        String dateOrder1 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        sleep(1000);

        action().click(page.myOrders);

        try {
            action().waitBy(page.orderedGeneralCleaning);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("В списке моих заказов не найден элемент заказанной услуги");
        }

        action().click(page.orderedGeneralCleaning);

        try {
            action().waitBy(By.xpath("//p[text()='Пожелание - заказ одной услуги по уборке']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("В карточке заказанной услуги комментарий не отобразился");
        }

        String dateOrder2 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();

        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата и время заказа не совпали");
        }

        String serviceOrderUrl = driver.getCurrentUrl();
        String[] segments = serviceOrderUrl.split("/");

        serviceOrderNumber = segments[segments.length - 1]; // обрезка всей строки до последнего слеша и получение номера заказа

        serviceOrderDate = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText().split("\n")[1];

        serviceOrderSum = action().findElement(By.xpath("//span[@class='ng-binding ng-scope']")).getText();

    }

    @Test(dependsOnMethods = "orderOneCleaningService", description = "Проверка создания заказа одной услуги по уборке в админке", priority = 2)
    public void newOneCleaningServiceInAdminPanel() throws InterruptedException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        page.authorizationInAdminPanel();

        action().click(page.dropDownMenuServices);

        try {
            action().waitBy(page.clientServicesSection);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Все заказы клиентов' не найден");
        }

        action().click(page.clientServicesSection);

        page.findServiceOrderNumberInAdminPanel();

        action().click(page.showServicesDetail);

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")), "innerText"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста в детализации заказа не найден");
        }

        sleep(500);

        String serviceOrderNumberAdmin = action().findElement(page.serviceOrderNumber).getText()
                .replace("250-", "").split("\n")[0];
        String serviceOrderNameAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")).getText();
        String serviceOrderSumAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[1]/div[3]")).getText()
                .replace(",00","");
        String serviceOrderDateAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]//div/span")).getText();
        String serviceOrderStatusAdmin = action().findElement(page.serviceOrderStatus).getText();
        String serviceOrderPayStatusAdmin = action().findElement(page.serviceOrderPayStatus).getText();

        try {
            Assert.assertTrue(serviceOrderNumber.contains(serviceOrderNumberAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер заказа в ЛК и в админке не совпал");
        }

        try {
            Assert.assertTrue(serviceOrderDate.contains(serviceOrderDateAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderNameAdmin.contains("Генеральная уборка"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Название услуги в ЛК и в админке не совпало");
        }

        try {
            Assert.assertTrue(serviceOrderSum.contains(serviceOrderSumAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сумма заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderStatusAdmin.contains("Не выполнен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заказа не верный");
        }

        try {
            Assert.assertTrue(serviceOrderPayStatusAdmin.contains("Не оплачен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус оплаты заказа не верный");
        }

    }

    @Test(description = "Заказ нескольких услуг по уборке", priority = 3)
    public void orderFewCleaningServices() throws InterruptedException {

        GlobalPage gloPage = new GlobalPage();
        OrderCleaningPage page = new OrderCleaningPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderCleaning);

        page.findOrderCleaningPageElement();

        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.maintainingCleaning);

        page.findMaintainingCleaningElement();

        new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='checkbox']/following-sibling::div")));
        driver.findElements(By.xpath("//input[@type='checkbox']/following-sibling::div")).stream().parallel().forEach(WebElement::click);

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));

        for (WebElement checkbox : checkboxes){

            try {
                Assert.assertTrue(checkbox.isSelected());
            } catch (AssertionError ex){
                action().afterExceptionMessage("Один или несколько чекбоксов не отмечены");
            }

        }

        action().click(page.checkboxWashDresserPlus);

        sleep(100);

        Assert.assertTrue(action().isElementPresent(By.xpath("//span[contains(text(),'2')]")));

        action().click(page.checkboxWashDresserPlus);

        sleep(100);

        Assert.assertTrue(action().isElementPresent(By.xpath("//span[contains(text(),'3')]")));

        action().click(page.checkboxWashDresserMinus);

        sleep(100);

        Assert.assertTrue(action().isElementPresent(By.xpath("//span[contains(text(),'2')]")));

        try {
            action().waitBy(page.chooseDateAndTimeElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы выбора даты и времени заказа не найден");
        }

        action().click(page.inputDate);

        sleep(100);

        action().click(page.chooseDate);

        action().click(page.inputTime);

        sleep(100);

        action().click(page.chooseTime);

        action().sendKeys(page.textAreaComments, "Пожелание - заказ нескольких услуг по уборке");

        action().click(page.submit);

        page.findServiceOrderedElement();

        String dateOrder1 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        action().click(page.myOrders);

        try {
            action().waitBy(page.orderedMaintainingCleaning);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("В списке моих заказов не найден элемент заказанной услуги");
        }

        action().click(page.orderedMaintainingCleaning);

        try {
            action().waitBy(By.xpath("//p[text()='Пожелание - заказ нескольких услуг по уборке']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("В карточке заказанной услуги комментарий не отобразился");
        }

        String dateOrder2 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();

        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата и время заказа не совпали");
        }

        String serviceOrderUrl = driver.getCurrentUrl();
        String[] segments = serviceOrderUrl.split("/");

        serviceOrderNumber = segments[segments.length - 1]; // обрезка всей строки до последнего слеша и получение номера заказа

        serviceOrderDate = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText().split("\n")[1];

        serviceOrderSum = action().findElement(By.xpath("//span[@class='ng-binding ng-scope']")).getText();

    }

    @Test(dependsOnMethods = "orderFewCleaningServices", description = "Проверка создания заказа нескольких услуг по уборке в админке", priority = 4)
    public void newFewCleaningServicesInAdminPanel() throws InterruptedException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        if (!action().elementIsNotPresent(page.userName)){

            page.authorizationInAdminPanel();

        }

        action().click(page.dropDownMenuServices);

        try {
            action().waitBy(page.clientServicesSection);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Все заказы клиентов' не найден");
        }

        action().click(page.clientServicesSection);

        page.findServiceOrderNumberInAdminPanel();

        action().click(page.showServicesDetail);

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")), "innerText"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста в детализации заказа не найден");
        }

        sleep(500);

        String serviceOrderNumberAdmin = action().findElement(page.serviceOrderNumber).getText()
                .replace("250-", "").split("\n")[0];
        String serviceOrderSumAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[1]/div[3]")).getText()
                .replace(",00","");
        String serviceOrderDateAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]//div/span")).getText();
        String serviceOrderStatusAdmin = action().findElement(page.serviceOrderStatus).getText();
        String serviceOrderPayStatusAdmin = action().findElement(page.serviceOrderPayStatus).getText();

        Set<String> nameServices = driver.findElements(By.xpath("//tbody/tr[1]/td[2]/div/small[1]"))
                .stream().map(element -> element.getText().trim()).collect(Collectors.toSet());

        for (String name : nameServices){

            if (name.contains("Поддерживающая уборка")){

                break;

            }

        }

        try {
            Assert.assertTrue(serviceOrderNumber.contains(serviceOrderNumberAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер заказа в ЛК и в админке не совпал");
        }

        try {
            Assert.assertTrue(serviceOrderDate.contains(serviceOrderDateAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderSum.contains(serviceOrderSumAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сумма заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderStatusAdmin.contains("Не выполнен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заказа не верный");
        }

        try {
            Assert.assertTrue(serviceOrderPayStatusAdmin.contains("Не оплачен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус оплаты заказа не верный");
        }

    }

}
