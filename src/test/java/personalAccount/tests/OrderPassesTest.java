package personalAccount.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import personalAccount.BaseTest;
import personalAccount.Configuration;
import personalAccount.framework.DriverManager;
import personalAccount.pageObjects.AdminPanelPage;
import personalAccount.pageObjects.AuthorizationPage;
import personalAccount.pageObjects.GlobalPage;
import personalAccount.pageObjects.OrderPassesPage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import static java.lang.Thread.sleep;

public class OrderPassesTest extends BaseTest {

    private String serviceOrderNumber;
    private String serviceOrderDate;
    private String serviceOrderSum;

    @BeforeTest
    public void beforeTest() throws InterruptedException {

        driver = new DriverManager().getDriver(Configuration.os, Configuration.browser);
        browser().maximize();

        AuthorizationPage auth = new AuthorizationPage(driver);

        driver.get(Configuration.url + "renter");

        auth.findAuthPageElement();

        auth.authorization();
    }

    @Test(description = "Хлебные крошки в заказе пропусков и ключей")
    public void breadcrumbsOrderPasses(){

        GlobalPage gloPage = new GlobalPage();
        OrderPassesPage page = new OrderPassesPage(driver);

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderPasses);

        page.findOrderPassesPageElement();

        // ожидание исчезновения спиннера
        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.oneTimePass);

        page.findOrderOneTimePassPageElement();

        action().sendKeys(page.textAreaNameGuest1, "Иванов");

        action().click(page.servicesBreadcrumbs);

        page.findOrderPassesPageElement();

        action().click(page.orderBreadcrumbs);

        String comment = action().findElement(By.xpath("//input")).getAttribute("value");

        try {
            Assert.assertEquals(comment, "Иванов");
        } catch (AssertionError ex){
            action().afterExceptionMessage("Полученный текст не совпадает с фамилией гостя");
        }

    }

    @Test(description = "Заказ одного бесплатного пропуска", priority = 1)
    public void orderOneFreePass() throws InterruptedException {

        GlobalPage gloPage = new GlobalPage();
        OrderPassesPage page = new OrderPassesPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderPasses);

        page.findOrderPassesPageElement();

        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.oneTimePass);

        page.findOrderOneTimePassPageElement();

        action().sendKeys(page.textAreaNameGuest1, "Иванов");

        action().click(page.inputDate);

        sleep(100);

        action().click(page.chooseDate);

        action().click(page.submit);

        page.findServiceOrderedElement();

        String dateOrder1 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        action().click(page.myOrders);

        try {
            action().waitBy(page.orderedOneTimeGuest);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент заказа в списке моих заказов не найден");
        }

        action().click(page.orderedOneTimeGuest);

        try {
            action().waitBy(By.xpath("//p[text()='Иванов']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("ФИО гостя не найдено");
        }

        String dateOrder2 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();

        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата и время заказа не совпали");
        }

        String serviceOrderUrl = driver.getCurrentUrl();
        String[] segments = serviceOrderUrl.split("/");

        serviceOrderNumber = segments[segments.length - 1]; // обрезка всей строки до последнего слеша и получение номера заказа

        serviceOrderDate = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText().split("\n")[1];

        serviceOrderSum = action().findElement(By.xpath("//span[@class='ng-binding ng-scope']")).getText();

    }

    @Test(dependsOnMethods = "orderOneFreePass", description = "Проверка создания заказа одного бесплатного пропуска в админке", priority = 2)
    public void newOneFreePassInAdminPanel() throws InterruptedException, ParseException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        page.authorizationInAdminPanel();

        action().click(page.dropDownMenuServices);

        try {
            action().waitBy(page.clientServicesSection);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Все заказы клиентов' не найден");
        }

        action().click(page.clientServicesSection);

        page.findServiceOrderNumberInAdminPanel();

        action().click(page.showServicesDetail);

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")), "innerText"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста в детализации заказа не найден");
        }

        sleep(500);

        String serviceOrderNumberAdmin = action().findElement(page.serviceOrderNumber).getText()
                .replace("250-", "").split("\n")[0];
        String serviceOrderNameAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")).getText();
        String serviceOrderSumAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[1]/div[3]")).getText()
                .replace(",00","");
        String dateAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small/span")).getText()
                .replace("Дата визита:", "").trim().split("\n")[2];
        String serviceOrderStatusAdmin = action().findElement(page.serviceOrderStatus).getText();
        String serviceOrderPayStatusAdmin = action().findElement(page.serviceOrderPayStatus).getText();

        // Смена формата даты
        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()); // старый формат
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd MMM", Locale.getDefault()); // новый формат
        Date date = oldDateFormat.parse(dateAdmin);
        String serviceOrderDateAdmin = newDateFormat.format(date);

        try {
            Assert.assertTrue(serviceOrderNumber.contains(serviceOrderNumberAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер заказа в ЛК и в админке не совпал");
        }

        try {
            Assert.assertTrue(serviceOrderDate.contains(serviceOrderDateAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderNameAdmin.contains("Разовый пропуск для гостей"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Название услуги в ЛК и в админке не совпало");
        }

        try {
            Assert.assertTrue(serviceOrderSum.contains(serviceOrderSumAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сумма заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderStatusAdmin.contains("Выполнен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заказа не верный");
        }

        try {
            Assert.assertTrue(serviceOrderPayStatusAdmin.contains("Бесплатно"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус оплаты заказа не верный");
        }

    }

    @Test(description = "Заказ нескольких бесплатных пропусков", priority = 3)
    public void orderFewFreePasses() throws InterruptedException {

        GlobalPage gloPage = new GlobalPage();
        OrderPassesPage page = new OrderPassesPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderPasses);

        page.findOrderPassesPageElement();

        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.oneTimePass);

        page.findOrderOneTimePassPageElement();

        action().sendKeys(page.textAreaNameGuest1, "Иванов");

        action().click(page.addMoreGuest);

        action().sendKeys(page.textAreaNameGuest2, "Петров");

        action().click(page.addMoreGuest);

        action().sendKeys(page.textAreaNameGuest3, "Сидоров");

        action().click(page.inputDate);

        sleep(100);

        action().click(page.chooseDate);

        action().click(page.submit);

        page.findServiceOrderedElement();

        String dateOrder1 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        action().click(page.myOrders);

        try {
            action().waitBy(page.orderedOneTimeGuest);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент заказа в списке моих заказов не найден");
        }

        action().click(page.orderedOneTimeGuest);

        try {
            action().waitBy(By.xpath("//p[text()='Иванов']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("ФИО гостя не найдено");
        }

        try {
            action().waitBy(By.xpath("//p[text()='Петров']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("ФИО гостя не найдено");
        }

        try {
            action().waitBy(By.xpath("//p[text()='Сидоров']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("ФИО гостя не найдено");
        }

        String dateOrder2 = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();

        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата и время заказа не совпали");
        }

        String serviceOrderUrl = driver.getCurrentUrl();
        String[] segments = serviceOrderUrl.split("/");

        serviceOrderNumber = segments[segments.length - 1]; // обрезка всей строки до последнего слеша и получение номера заказа

        serviceOrderDate = action().findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText().split("\n")[1];

        serviceOrderSum = action().findElement(By.xpath("//span[@class='ng-binding ng-scope']")).getText();

    }

    @Test(dependsOnMethods = "orderFewFreePasses", description = "Проверка создания заказа нескольких бесплатных пропусков в админке", priority = 4)
    public void newFewFreePassesInAdminPanel() throws InterruptedException, ParseException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        if (!action().elementIsNotPresent(page.userName)){

            page.authorizationInAdminPanel();

        }

        action().click(page.dropDownMenuServices);

        try {
            action().waitBy(page.clientServicesSection);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Все заказы клиентов' не найден");
        }

        action().click(page.clientServicesSection);

        page.findServiceOrderNumberInAdminPanel();

        action().click(page.showServicesDetail);

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")), "innerText"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста в детализации заказа не найден");
        }

        sleep(500);

        String serviceOrderNumberAdmin = action().findElement(page.serviceOrderNumber).getText()
                .replace("250-", "").split("\n")[0];
        String[] serviceOrderNameAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div/small[1]")).getText()
                .split("\n");
        String serviceOrderSumAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[1]/div[3]")).getText()
                .replace(",00","");
        String dateAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small/span")).getText()
                .replace("Дата визита:", "").trim().split("\n")[4];
        String serviceOrderStatusAdmin = action().findElement(page.serviceOrderStatus).getText();
        String serviceOrderPayStatusAdmin = action().findElement(page.serviceOrderPayStatus).getText();

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());
        Date date = oldDateFormat.parse(dateAdmin);
        String serviceOrderDateAdmin = newDateFormat.format(date);

        try {
            Assert.assertTrue(serviceOrderNumber.contains(serviceOrderNumberAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер заказа в ЛК и в админке не совпал");
        }

        try {
            Assert.assertTrue(serviceOrderNameAdmin[2].contains("Иванов"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Фамилия гостя в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderNameAdmin[3].contains("Петров"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Фамилия гостя в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderNameAdmin[4].contains("Сидоров"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Фамилия гостя в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderDate.contains(serviceOrderDateAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Дата заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderSum.contains(serviceOrderSumAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сумма заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderStatusAdmin.contains("Выполнен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заказа не верный");
        }

        try {
            Assert.assertTrue(serviceOrderPayStatusAdmin.contains("Бесплатно"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус оплаты заказа не верный");
        }

    }

    @Test(description = "Заказ платного пропуска", priority = 5)
    public void orderPayPass(){

        GlobalPage gloPage = new GlobalPage();
        OrderPassesPage page = new OrderPassesPage(driver);

        driver.get(Configuration.url + "renter");

        while (!action().isElementPresent(gloPage.apart250)) {

            action().click(gloPage.nextObject);

        }

        action().click(page.orderPasses);

        page.findOrderPassesPageElement();

        action().waitForElementIsInvisibility(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        action().click(page.payPass);

        try {
            action().waitBy(page.payPassElement);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент страницы заказа пропуска не найден");
        }

        action().click(page.submit);

        page.findServiceOrderedElement();

        action().click(page.myOrders);

        try {
            action().waitBy(page.orderedPayPass);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент заказа в списке моих заказов не найден");
        }

        action().click(page.orderedPayPass);

        try {
            action().waitBy(By.xpath("//p[text()='Мифтахутдинов Рамиль Рустэмович']"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("ФИО арендатора не найдено");
        }

        String serviceOrderUrl = driver.getCurrentUrl();
        String[] segments = serviceOrderUrl.split("/");

        serviceOrderNumber = segments[segments.length - 1]; // обрезка всей строки до последнего слеша и получение номера заказа

        serviceOrderSum = action().findElement(By.xpath("//span[@class='ng-binding ng-scope']")).getText();

    }

    @Test(dependsOnMethods = "orderPayPass", description = "Проверка создания заказа платного пропуска в админке", priority = 6)
    public void newOnePayPassInAdminPanel() throws InterruptedException {

        AdminPanelPage page = new AdminPanelPage(driver);

        driver.get(Configuration.url + "admin");

        if (!action().elementIsNotPresent(page.userName)){

            page.authorizationInAdminPanel();

        }

        action().click(page.dropDownMenuServices);

        try {
            action().waitBy(page.clientServicesSection);
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент раздела 'Все заказы клиентов' не найден");
        }

        action().click(page.clientServicesSection);

        page.findServiceOrderNumberInAdminPanel();

        action().click(page.showServicesDetail);

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")), "innerText"));
        } catch (TimeoutException ex){
            action().afterExceptionMessage("Элемент текста в детализации заказа не найден");
        }

        sleep(500);

        String serviceOrderNumberAdmin = action().findElement(page.serviceOrderNumber).getText()
                .replace("250-", "").split("\n")[0];
        String[] serviceOrderNameAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small[1]")).getText().split("\n");
        String serviceOrderSumAdmin = action().findElement(By.xpath("//tbody/tr[1]/td[2]/div[1]/div[3]")).getText()
                .replace(",00","");
        String serviceOrderStatusAdmin = action().findElement(page.serviceOrderStatus).getText();
        String serviceOrderPayStatusAdmin = action().findElement(page.serviceOrderPayStatus).getText();

        try {
            Assert.assertTrue(serviceOrderNumber.contains(serviceOrderNumberAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Номер заказа в ЛК и в админке не совпал");
        }

        try {
            Assert.assertTrue(serviceOrderNameAdmin[0].contains("Восстановление постоянного пропуска"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Название услуги в ЛК и в админке не совпало");
        }

        try {
            Assert.assertTrue(serviceOrderNameAdmin[2].contains("Мифтахутдинов Рамиль Рустэмович"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Название услуги в ЛК и в админке не совпало");
        }

        try {
            Assert.assertTrue(serviceOrderSum.contains(serviceOrderSumAdmin));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Сумма заказа в ЛК и в админке не совпала");
        }

        try {
            Assert.assertTrue(serviceOrderStatusAdmin.contains("Не выполнен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус заказа не верный");
        }

        try {
            Assert.assertTrue(serviceOrderPayStatusAdmin.contains("Не оплачен"));
        } catch (AssertionError ex){
            action().afterExceptionMessage("Статус оплаты заказа не верный");
        }

    }

}
