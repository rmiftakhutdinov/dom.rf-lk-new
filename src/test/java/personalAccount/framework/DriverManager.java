package personalAccount.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import static personalAccount.Configuration.browser;

public class DriverManager {

    public WebDriver getDriver(String operationSystem, String browser){

        String os = operationSystem.toLowerCase();
        String driver = browser.toLowerCase();

        switch (driver){
            case "chrome":
                switch (os) {
                    case "windows":
                        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver_windows.exe");
                        break;
                    case "linux":
                        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver_linux");
                        break;
                    case "macos":
                        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver_macos");
                        break;
                    default:
                        throw new RuntimeException("Wrong os variable. Please use windows, linux or macos value.");
                }
                return new ChromeDriver();
            case "firefox":
                switch (os) {
                    case "windows":
                        System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver_windows.exe");
                        break;
                    case "linux":
                        System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver_linux");
                        break;
                    case "macos":
                        System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver_macos");
                        break;
                    default:
                        throw new RuntimeException("Wrong os variable. Please use windows, linux or macos value.");
                }
                System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
                System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
                return new FirefoxDriver();
            default:
                throw new RuntimeException("Wrong browser variable. Please use chrome or firefox value");
        }
    }

    public WebDriver getDriver(String operationSystem){

        String os = operationSystem.toLowerCase();
        String driver = browser.toLowerCase();

        switch (driver){
            case "chrome":
                switch (os) {
                    case "windows":
                        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver_windows.exe");
                        break;
                    case "linux":
                        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver_linux");
                        break;
                    case "macos":
                        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver_macos");
                        break;
                    default:
                        throw new RuntimeException("Wrong os variable. Please use windows, linux or macos value.");
                }
                return new ChromeDriver();
            case "firefox":
                switch (os) {
                    case "windows":
                        System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver_windows.exe");
                        break;
                    case "linux":
                        System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver_linux");
                        break;
                    case "macos":
                        System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver_macos");
                        break;
                    default:
                        throw new RuntimeException("Wrong os variable. Please use windows, linux or macos value.");
                }
                System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
                System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
                return new FirefoxDriver();
            default:
                throw new RuntimeException("Wrong browser variable. Please use chrome or firefox value");
        }
    }

}
