package personalAccount.framework.helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.Dimension;
import java.awt.*;

public class BrowserHelper extends BaseHelper {

    public BrowserHelper(WebDriver driver){
        super(driver);
    }

    public void maximize(){
        driver.manage().window().maximize();
    }

    public void setDimension(int width, int height){
        Dimension dimension = new Dimension(width, height);
        driver.manage().window().setSize(dimension);
    }

    public Dimension getCurrentDimension(){
        return driver.manage().window().getSize();
    }

}
