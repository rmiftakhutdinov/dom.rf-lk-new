package personalAccount.framework.helpers;

import org.openqa.selenium.WebDriver;

import java.net.URL;

public class NavigationHelper extends BaseHelper {

    public NavigationHelper(WebDriver driver){
        super(driver);
    }

    public void page(String pageURL){

        if (!driver.getCurrentUrl().equals(pageURL)){
            driver.navigate().to(pageURL);
        }

    }

    public void page(URL pageURL){

        if (!driver.getCurrentUrl().equals(pageURL.toString())) {
            driver.navigate().to(pageURL);
        }
    }

    public void back(){
        driver.navigate().back();
    }

    public void refresh(){
        driver.navigate().refresh();
    }

    public void forward(){
        driver.navigate().forward();
    }

}
