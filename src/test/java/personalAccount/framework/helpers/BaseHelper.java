package personalAccount.framework.helpers;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.util.Random;

public class BaseHelper {

    protected WebDriver driver;

    public BaseHelper(WebDriver driver){
        this.driver = driver;
    }

    public void sendText(By by, String text){

        WebElement element = findElement(by); // search web element

        if (!element.getAttribute("value").equals(text)){

            element.clear();

            element.sendKeys(text);

        }

    }

    public void sendText(WebElement element, String text){

        if (!element.getAttribute("value").equals(text)){

            element.clear();

            element.sendKeys(text);

        }

    }

    public void sendKey(By by, Keys key){

        WebElement element = findElement(by); // search web element

        element.sendKeys(key);

    }

    public void sendKeys(By by, String string){

        findElement(by).clear();
        findElement(by).sendKeys(string); // Вставить текст

    }

    public void sendKeys(By by, Keys keys){

        findElement(by).clear();
        findElement(by).sendKeys(keys); // Нажать с клавиатуры

    }

    public void dragAndDrop(By element, By target){

        WebElement e1 = findElement(element);
        WebElement e2 = findElement(target);

        new Actions(driver).dragAndDrop(e1, e2).perform();
    }

    public void dragAndDrop(WebElement element, WebElement target){

        new Actions(driver).dragAndDrop(element, target).perform();
    }

    public void afterExceptionMessage(String message){

        throw new TimeoutException("\n" + message); // Сообщение об ошибке

    }

    public void sleep(int time) throws InterruptedException {

        Thread.sleep(time); // Таймаут по заданному времени

    }

    public void click(By by){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(by)).click();
        } catch (TimeoutException ex){
            throw new RuntimeException("Элемент " + by + " не найден на странице или не кликабелен");
        }

    }

    public void waitBy(By by, int time){

        new WebDriverWait(driver, time)
                .until(ExpectedConditions.visibilityOfElementLocated(by)); // Ожидание элемента с выбором времени

    }

    public void waitBy(By by){

        new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOfElementLocated(by)); // Ожидание элемента с установленным по дефолту временем

    }

    public WebElement findElement(By by){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(by)); // Поиск и ожидание элемента
        } catch (java.util.NoSuchElementException ex){
            throw new RuntimeException("Элемент " + by + " не найден на странице");
        }

        return driver.findElement(by);

    }

    public WebElement findElement(By by, int time){

        try {
            new WebDriverWait(driver, time).until(ExpectedConditions.presenceOfElementLocated(by)); // Поиск и ожидание элемента
        } catch (java.util.NoSuchElementException ex){
            throw new RuntimeException("Элемент " + by + " не найден на странице");
        }

        return driver.findElement(by);

    }

    public void scrollToElement(By by) throws InterruptedException {

        WebElement element = driver.findElement(by);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element); // Скролл к элементу
        sleep(1000);

    }

    public boolean isElementPresent(By selector){

        try{
            driver.findElement(selector); // Проверка видимости элемента на странице (не убирать 'driver')
            return true;
        } catch (NoSuchElementException ex){
            return false;
        }

    }

    public boolean elementIsNotPresent(By selector){

        try{

            int timeouts = 5;
            new WebDriverWait(driver, timeouts).until(ExpectedConditions.presenceOfElementLocated(selector));
            return false;

        } catch (TimeoutException ex){

            return true;

        }

    }

    public void setClipboardData(String string) {

        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null); // Добавление файла

    }

    public void waitForElementIsInvisibility(By by){

        new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOfElementLocated(by));

    }

    public void waitForElementIsInvisibility(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOfElementLocated(by));

    }

    // Выбор селекта по value
    public void selectByValue(By by, String string){

        Select select = new Select(findElement(by));
        select.selectByValue(string);

    }

    // Выбор селекта по тексту
    protected void selectByText(By by, String string){

        Select select = new Select(findElement(by));
        select.selectByVisibleText(string);

    }

    // Генерация случайных символов
    protected String randomString(String data, int length){

        /**
         * @data - принимает на вход символьный набор, из которого генерится строка
         * @length - длина выходного значения
         */

        StringBuffer str = new StringBuffer();
        for (int i=0; i < length; i++){

            str.append(data.charAt(new Random().nextInt(data.length())));

        }

        return str.toString();

    }

    // Ожидание исчезновения элемента из дома
    protected void domElementIsNotVisible(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.stalenessOf(findElement(by)));

    }

    // Ожидание отрезка URL страницы
    protected boolean urlContains(String phrase){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(phrase));
            return true;
        } catch (TimeoutException ex){
            return false;
        }

    }

}
