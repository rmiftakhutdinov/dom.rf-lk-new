package personalAccount.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ObjectsNoDataPage {

    public By nextObject = By.xpath("//img[@src='./images/appartment-arrow-right@2x.png']");

    public By parkingA71 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'A71')]");

    public By myOrders = By.xpath("//p[text()='Мои заказы']");

    public By myOrdersElement = By.xpath("//h1[text()='Мои заказы']");

    public By orderCleaning = By.xpath("//div[contains(@ui-sref,'clean')]/p");

    public By cleaningElement = By.xpath("//h1[text()='Заказ уборки']");

    public By orderRepair = By.xpath("//div[contains(@ui-sref,'repair')]/p");

    public By repairElement = By.xpath("//h1[text()='Заказ ремонта']");

    public By orderPassesAndKeys = By.xpath("//p[contains(text(),'Пропуска')]");

    public By passesAndKeysElement = By.xpath("//h1[text()='Заказ пропусков и ключей']");

    public By orderOtherServices = By.xpath("//p[contains(text(),'Прочие')]");

    public By otherServicesElement = By.xpath("//h1[text()='Заказ прочих услуг']");

    public By paymentHistory = By.xpath("//div[contains(@ui-sref,'paymentHistory')]/p");

    public By paymentHistoryElement = By.xpath("//p[@ng-bind-html='$ctrl.trs.paymentHistory.noHistory']");

    public By rent = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Аренда']");

    public By rentElement = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope payments_filter-item--active'][text()='Аренда']");

    public By communal = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Коммунальные']");

    public By communalElement = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope payments_filter-item--active'][text()='Коммунальные']");

    public By services = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Услуги']");

    public By servicesElement = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope payments_filter-item--active'][text()='Услуги']");

    public By other = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Прочие']");

    public By otherElement = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope payments_filter-item--active'][text()='Прочие']");

    public By allPayments = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Все']");

    public By allPaymentsElement = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope payments_filter-item--active'][text()='Все']");

    public By consumptionHistory = By.xpath("//div[contains(@ui-sref,'consHistory')]/p");

    public By consumptionHistoryElement = By.xpath("//h1[text()='История потребления']");

    public By electric = By.xpath("//div[@class='cons-history_filter']/a[contains(text(),'Электричество')]");

    public By coldWater = By.xpath("//div[@class='cons-history_filter']/a[contains(text(),'Холодная')]");

    public By hotWater = By.xpath("//div[@class='cons-history_filter']/a[contains(text(),'Горячая')]");

    public By noHistoryElement = By.xpath("//p[contains(text(),'Нет потреблений')]");

    public By menu = By.xpath("//div[@class='lk-header_nav']");

    public By payments = By.xpath("//a[contains(text(),'Счета к оплате')]");

    public By noPaymentsElement = By.xpath("//p[text()='Нет счетов. Счета за аренду и коммунальные услуги формируются раз в месяц.']");

    public By noApartmentElement = By.xpath("//p[text()='Нет активных договоров аренды']");

    public By terminalRoscap = By.xpath("//div[@class='payments_filter']/a[contains(text(),'Аренда')]");

    public By terminalPayture = By.xpath("//div[@class='payments_filter']/a[contains(text(),'Услуги')]");


    // Инициализация драйвера
    private WebDriver driver;
    public ObjectsNoDataPage(WebDriver driver){
        this.driver = driver;
    }

    // Поиск элемента раздела "Мои заказы"
    public void findMyOrdersPageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(myOrdersElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент раздела 'Мои заказы' не найден");
        }

    }

    // Поиск элемента текста об отсутствии потреблений
    public void findNoConsumptionHistoryElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(noHistoryElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент текста об отсутствии потреблений не найден");
        }

    }

}
