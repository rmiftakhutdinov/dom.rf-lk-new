package personalAccount.pageObjects;

import org.openqa.selenium.By;

public class FAQPage {

    public By FAQ = By.xpath("//p[contains(text(),'Помощь')]");

    public By FAQElement = By.xpath("//h1[text()='Помощь и контакты']");

    public By parking = By.xpath("//p[text()='Предусмотрена ли парковка']");

    public By parkingElement = By.xpath("//p[@class='ng-binding ng-scope'][contains(text(),'Для арендных апартаментов')]");

    public By apartments = By.xpath("//label[@class='faq-questions-title ng-binding ng-scope'][text()='Апартаменты']");

    public By internet = By.xpath("//p[text()='Есть ли Интернет в апартаментах']");

    public By internetElement = By.xpath("//p[@class='ng-binding ng-scope'][contains(text(),'Во всех апартаментах проведен')]");

}
