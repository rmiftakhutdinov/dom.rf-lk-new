package personalAccount.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static java.lang.Thread.sleep;

public class AdminPanelPage {

    // Все заказы клиентов
    public By userName = By.xpath("//input[@id='inputEmail']");
    private By userPassword = By.xpath("//input[@id='inputPassword']");
    private By submit = By.xpath("//button");
    private By filterElement = By.xpath("//div[@class='search-input-wrapper multi-filter-wrapper ng-scope search-filter']");
    private By objectNumberElement = By.xpath("//tbody/tr[1]/td[2]/a");
    public By dropDownMenuServices = By.xpath("//a[@class='dropdown-toggle'][@name='qa-head-all-services']");
    public By clientServicesSection = By.xpath("//a[text()='Все заказы клиентов']");
    public By serviceOrderNumber = By.xpath("//tbody/tr[1]/td[1]");
    public By showServicesDetail = By.xpath("//tbody/tr[1]/td[2]/a[contains(text(),'Детально')]");
    public By serviceOrderStatus = By.xpath("//tr[1]//td[4]//span[1]");
    public By serviceOrderPayStatus = By.xpath("//tr[1]//td[4]//div[1]//small[1]");


    // Инициализация драйвера
    private WebDriver driver;
    public AdminPanelPage(WebDriver driver){
        this.driver = driver;
    }

    // Авторизация в Админке: user admin@example.com
    public void authorizationInAdminPanel() throws InterruptedException {

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(submit));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы авторизации не найден");
        }

        driver.findElement(userName).clear();
        driver.findElement(userName).sendKeys("admin@example.com");
        driver.findElement(userPassword).clear();
        driver.findElement(userPassword).sendKeys("spend-table-tttt-alcoa");

        sleep(1000);

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(submit)).click();

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(filterElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент строки поиска в списке апартаментов не найден");
        }

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(objectNumberElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент апартамента в списке апартаментов не найден");
        }

    }

    // Ожидание элемента номера заказа в разделе 'Все заказы клиентов'
    public void findServiceOrderNumberInAdminPanel(){

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(serviceOrderNumber));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент раздела 'Все заказы клиентов' не найден");
        }

    }

}
