package personalAccount.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderPassesPage {

    public By orderPasses = By.xpath("//p[contains(text(),'Пропуска')]");

    public By orderPassesElement = By.xpath("//h1[text()='Заказ пропусков и ключей']");

    public By oneTimePass = By.xpath("//p[text()='Разовый пропуск для гостей']");

    public By payPass = By.xpath("//p[text()='Восстановление постоянного пропуска']");

    public By payPassElement = By.xpath("//h2[text()='Восстановление постоянного пропуска']");

    public By textAreaNameGuest1 = By.xpath("//div[@class='services-wishes ng-scope']/input[1]");

    public By textAreaNameGuest2 = By.xpath("//div[@class='services-wishes ng-scope']/input[2]");

    public By textAreaNameGuest3 = By.xpath("//div[@class='services-wishes ng-scope']/input[3]");

    public By submit = By.xpath("//button");

    public By addMoreGuest = By.xpath("//p[text()='+ Добавить еще одного гостя']");

    public By inputDate = By.xpath("//li[@class='btn btn-default btn-xs select-search-list-item select-search-list-item_selection ng-binding ng-scope']");

    public By chooseDate = By.xpath("//oi-select[@class='ng-pristine ng-untouched ng-valid ng-scope ng-isolate-scope ng-not-empty focused open']/div[@class='select-dropdown']/ul/li[3]");

    public By myOrders = By.xpath("//button[text()='Мои заказы']");

    public By serviceOrderedElement = By.xpath("//p[contains(text(),'Заказ оформлен.')]");

    public By orderBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Заказ']");

    public By servicesBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Услуги']");

    public By orderedOneTimeGuest = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Разовый пропуск для гостей']");

    public By orderedPayPass = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Восстановление постоянного пропуска']");


    // Инициализация драйвера
    private WebDriver driver;
    public OrderPassesPage(WebDriver driver){
        this.driver = driver;
    }

    // Ожидание элемента страницы 'Пропуска и ключи'
    public void findOrderPassesPageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(orderPassesElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы 'Пропуска и ключи' не найден");
        }

    }

    // Ожидание элемента страницы заказа одноразового пропуска
    public void findOrderOneTimePassPageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(addMoreGuest));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы заказа одноразового пропуска не найден");
        }

    }

    // Ожидание элемента страницы успешного заказа
    public void findServiceOrderedElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(serviceOrderedElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы успешного заказа не найден");
        }

    }

}
