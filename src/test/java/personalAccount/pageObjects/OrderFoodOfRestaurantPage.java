package personalAccount.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderFoodOfRestaurantPage {

    public By orderFood = By.xpath("//div[contains(@ng-click,'modalFood')]");

    public By modalFoodElement = By.xpath("//h2[@class='food-delivery-modal_title']");

    public By deliverToApartment = By.xpath("//div[contains(text(),'Доставить в апартамент')]");

    public By reserveTable = By.xpath("//div[contains(text(),'Забронировать столик')]");

    public By pickYourself = By.xpath("//div[contains(text(),'Забрать самостоятельно')]");

    public By menu = By.xpath("//h2[contains(text(),'Меню ресторана')]");

    public By bar = By.xpath("//button[contains(text(),'Бар')]");

    public By kitchen = By.xpath("//button[contains(text(),'Кухня')]");

    public By breadcrumbsMainMenu = By.xpath("//a[contains(text(),'Главное меню')]");

    public By breadcrumbsMenu = By.xpath("//a[@ui-sref='foodList']");

    public By breadcrumbsCart = By.xpath("//a[@class='ng-binding ng-scope']");

    public By submitToCart = By.xpath("//button[contains(text(),'Перейти к оформлению')]");

    public By barTea = By.xpath("//div[@class='food-list--menu menu-bar active']//div[1]/ul//li[1]//div[@class='plus']");
    public By barTeaDropDown = By.xpath("//div[@class='food-list--menu menu-bar active']//div[1]/ul//li[1]//div[@class='dropdown-toggle ng-scope']");
    public By openDropDownMenu = By.xpath("//div[@class='dropdown-menu menu-adds open']");
    public By teaMoreLemon = By.xpath("//label[contains(text(),'лимон')]");
    public By teaMoreHoney = By.xpath("//label[contains(text(),'мед')]");

    public By barCoffee = By.xpath("//div[@class='food-list--menu menu-bar active']//div[3]/ul//li[1]//div[@class='plus']");
    public By barCoffeeDropDown = By.xpath("//div[@class='food-list--menu menu-bar active']//div[3]/ul//li[1]//div[@class='dropdown-toggle ng-scope']");
    public By coffeeMoreCoco = By.xpath("//label[contains(text(),'Сироп: кокос, 50 \u20BD')]");
    public By coffeeMoreVanilla = By.xpath("//label[contains(text(),'Сироп: ваниль, 50 \u20BD')]");

    public By barJuice = By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='plus']");

    public By chooseDateAndTimePageElement = By.xpath("//h1[contains(text(),'Оформление заказа')]");
    public By chooseDateAndTimeReserveTableElement = By.xpath("//h1[contains(text(),'Забронировать столик в ресторане')]");

    public By menuBarActive = By.xpath("//div[@class='food-list--menu menu-bar active']");
    public By menuKitchenActive = By.xpath("//div[@class='food-list--menu menu-kitchen active']");

    public By kitchenSoup = By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='plus']");
    public By kitchenBurger = By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='plus']");

    public By date = By.xpath("//service-select[@type=\"'date'\"]");
    public By time = By.xpath("//service-select[@type=\"'time'\"]");
    public By chooseDateOrTime = By.xpath("//ul[@class='select-dropdown-optgroup ng-scope']/li[5]");

    public By comment = By.xpath("//textarea[@name='comment']");

    public By submit = By.xpath("//div[@class='btn btn--l btn--green']");
    public By submitReserveTable = By.xpath("//button[@class='btn btn--l btn--green']");

    public By orderAccepted = By.xpath("//span[contains(text(),'Ваш заказ принят')]");
    public By managerConfirmOrder = By.xpath("//p[contains(text(),'В ближайшее время с вами свяжется менеджер')]");

    public By teaInCart = By.xpath("//ul[@class='menu-category--sub']//li[1]//div[@class='plus']");
    public By coffeeInCart = By.xpath("//ul[@class='menu-category--sub']//li[2]//div[@class='plus']");
    public By burgerInCart = By.xpath("//ul[@class='menu-category--sub']//li[5]//div[@class='plus']");

    public By teaCartDropDown = By.xpath("//ul//li[1]//div[@class='dropdown-toggle ng-scope']");
    public By coffeeCartDropDown = By.xpath("//ul//li[2]//div[@class='dropdown-toggle ng-scope']");

    public By teaMoreTeapot1Element = By.xpath("//div[contains(text(),'1 чайник')]");
    public By teaMoreTeapot2Element = By.xpath("//div[contains(text(),'2 чайник')]");

    public By coffeeMoreCup1Element = By.xpath("//div[contains(text(),'1 чашка')]");
    public By coffeeMoreCup2Element = By.xpath("//div[contains(text(),'2 чашка')]");
    public By burgerMoreElement = By.xpath("//ul[@class='menu-category--sub']//li[5]//div[text()='2']");


    // Инициализация драйвера
    private WebDriver driver;
    public OrderFoodOfRestaurantPage(WebDriver driver){
        this.driver = driver;
    }

    // Ожидание элемента модального окна
    public void findModalElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(modalFoodElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент модального окна не найден");
        }

    }

    // Ожидание элемента меню ресторана
    public void findMenuRestaurantElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(menu));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент меню ресторана не найден");
        }

    }

    // Ожидание элемента меню бара
    public void findMenuBarElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(menuBarActive));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент меню бара не найден");
        }

    }

    // Ожидание элемента меню кухни
    public void findMenuKitchenElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(menuKitchenActive));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент меню кухни не найден");
        }

    }

    // Ожидание элемента хлебной крошки корзины
    public void findBreadcrumbsCartElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(breadcrumbsCart));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент хлебной крошки 'Корзина' не найден");
        }

    }

    // Ожидание элемента страницы оформления заказа
    public void findCheckoutPageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(chooseDateAndTimePageElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы оформления заказа не найден");
        }

    }

    // Ожидание первого элемента страницы успешного заказа
    public void findOrderAcceptedElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(orderAccepted));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы успешного заказа не найден");
        }

    }

    // Ожидание второго элемента страницы успешного заказа
    public void findManagerConfirmOrderElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(managerConfirmOrder));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы успешного заказа не найден");
        }

    }

}
