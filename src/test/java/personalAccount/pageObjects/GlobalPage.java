package personalAccount.pageObjects;

import org.openqa.selenium.By;

public class GlobalPage {

    public By nextObject = By.xpath("//img[@src='./images/appartment-arrow-right@2x.png']");

    public By apart250 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'250')]");

    public By parkingA70 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'A70')]");

    public By apart220 = By.xpath("//h1[@class='lk-cover_title ng-binding'][contains(text(),'220')]");

    public By apart221 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'221')]");

    public By apart222 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'222')]");


}
