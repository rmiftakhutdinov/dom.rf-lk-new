package personalAccount.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderCleaningPage {

    public By orderCleaning = By.xpath("//div[contains(@ui-sref,'clean')]/p");

    public By orderCleaningElement = By.xpath("//h1[text()='Заказ уборки']");

    public By maintainingCleaning = By.xpath("//p[text()='Поддерживающая уборка']");

    public By maintainingCleaningElement = By.xpath("//h2[text()='Поддерживающая уборка']");

    public By generalCleaning = By.xpath("//p[text()='Генеральная уборка']");

    public By generalCleaningElement = By.xpath("//h2[text()='Генеральная уборка']");

    public By checkboxWashDresserPlus = By.xpath("//div[@class='services-sub_item ng-scope active'][3]//div[@class='services-multi ng-scope']/span[2]");

    public By checkboxWashDresserMinus = By.xpath("//div[@class='services-sub_item ng-scope active'][3]//div[@class='services-multi ng-scope']/span[1]");

    public By textAreaComments = By.xpath("//textarea[@name='additional_wishes']");

    public By submit = By.xpath("//button");

    public By serviceOrderedElement = By.xpath("//p[contains(text(),'Заказ оформлен.')]");

    public By chooseDateAndTimeElement = By.xpath("//label[@class='ng-binding'][text()='Выберите удобный день и время']");

    public By inputDate = By.xpath("//service-select[@class='services-select_date ng-isolate-scope']/oi-select");

    public By chooseDate = By.xpath("//service-select[@class='services-select_date ng-isolate-scope']//div[@class='select-dropdown']/ul/li[3]");

    public By chooseTime = By.xpath("//service-select[@class='services-select_time ng-isolate-scope']//div[@class='select-dropdown']/ul/li[3]");

    public By inputTime = By.xpath("//service-select[@class='services-select_time ng-isolate-scope']/oi-select");

    public By myOrders = By.xpath("//button[text()='Мои заказы']");

    public By orderBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Заказ']");

    public By servicesBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Услуги']");

    public By orderedGeneralCleaning = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Генеральная уборка']");

    public By orderedMaintainingCleaning = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Уборка, 11 услуг']");

    public By cleaningDetailShow = By.xpath("//div[@class='services-clean-details__toggler']");

    public By cleaningDetailClose = By.xpath("//div[@class='services-clean-details__toggler shown']");

    public By generalDetailInfo = By.xpath("//p[contains(text(),'Удаление налета и водного камня')]");

    public By maintainingDetailInfo = By.xpath("//p[contains(text(),'Влажная протирка свободной поверхности пола')]");


    // Инициализация драйвера
    private WebDriver driver;
    public OrderCleaningPage(WebDriver driver){
        this.driver = driver;
    }

    // Ожидание элемента страницы 'Заказать уборку'
    public void findOrderCleaningPageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(orderCleaningElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы 'Заказать уборку' не найден");
        }

    }

    // Ожидание элемента генеральной уборки
    public void findGeneralCleaningElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(generalCleaningElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы заказа генеральной уборки не найден");
        }

    }

    // Ожидание элемента поддерживающей уборки
    public void findMaintainingCleaningElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(maintainingCleaningElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы заказа поддерживающей уборки не найден");
        }

    }

    // Ожидание элемента страницы успешного заказа
    public void findServiceOrderedElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(serviceOrderedElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы успешного заказа не найден");
        }

    }

}
