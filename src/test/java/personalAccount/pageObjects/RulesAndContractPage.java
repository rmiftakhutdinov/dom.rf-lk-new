package personalAccount.pageObjects;

import org.openqa.selenium.By;

public class RulesAndContractPage {

    public By rulesAndContract = By.xpath("//p[contains(text(),'Условия')]");

    public By rulesAndContractElement = By.xpath("//h1[text()='Условия и договор']");

    public By rulesOfResidence = By.xpath("//a[@class='link--green docs_link docs_link--rules ng-binding ng-scope'][text()='Правила проживания']");

    public By contractOfRent = By.xpath("//a[contains(text(),'Договор аренды до')]");

    public By contractOfRentElement = By.xpath("//h1[contains(text(),'Договор аренды до')]");

    public By breadcrumbs = By.xpath("//docs-galery/a[text()='Условия и договор']");

    public By img = By.xpath("//figure[1]/a/img");

    public By pdf = By.xpath("//a/span");

    public By openJPG = By.xpath("//img[@class='pswp__img'][contains(@src,'jpeg')]");

    public By openPNG = By.xpath("//img[@class='pswp__img'][contains(@src,'png')]");

    public By nextImg = By.xpath("//button[@class='pswp__button pswp__button--arrow--right']");

    public By prevImg = By.xpath("//button[@class='pswp__button pswp__button--arrow--left']");

    public By closeImg = By.xpath("//button[@class='pswp__button pswp__button--close']");

}
