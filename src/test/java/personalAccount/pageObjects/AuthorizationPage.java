package personalAccount.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import personalAccount.data.CommonData;
import static java.lang.Thread.sleep;

public class AuthorizationPage {

    public By userName = By.xpath("//input[@name='username']");

    public By userPassword = By.xpath("//input[@name='password']");

    public By submit = By.xpath("//button");

    public By homePageElement = By.xpath("//div[@class='title ng-binding'][text()='Лайнер']");

    public By authPageElement = By.xpath("//h1[@class='auth-page-caption ng-binding'][text()='Вход в личный кабинет жильца']");

    public By logo = By.xpath("//a[@class='auth-page-logo logo']");

    public By cabinetElement = By.xpath("//div[@class='lk-header_setting icon-settings']");

    public By changePassword = By.xpath("//a[@class='link--green link--border change-password_link ng-binding'][text()='Забыли пароль?']");

    public By backToAuthPage = By.xpath("//a[@class='link--green link--border change-password_link ng-binding'][text()='Войти с паролем']");

    public By nextPage = By.xpath("//button[text()='Далее']");

    public By sendCodPage = By.xpath("//label[text()='Код подтверждения']");

    public By sendCodAgain = By.xpath("//button[text()='Запросить код еще раз']");

    public By changePhoneNumber = By.xpath("//a[@class='link--green link--border change-password_link ng-binding'][text()='Изменить номер']");

    public By sendLinkAgain = By.xpath("//button[@button-spinner='checkingUserName'][text()='Отправить еще раз']");

    public By input = By.xpath("//input");

    public By textRequestCodAgain = By.xpath("//span[contains(text(),'Запросить код еще раз можно')]");

    public By spinner = By.xpath("//div[@class='button-spinner-container']");

    public By eye = By.xpath("//div[@class='s-ico s-ico-eye input-eye']");

    public By termsOfUse = By.xpath("//a[text()='Условиями использования']");


    // Инициализация драйвера
    private WebDriver driver;
    public AuthorizationPage(WebDriver driver){
        this.driver = driver;
    }

    // Ожидание элемента страницы авторизации
    public void findAuthPageElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(authPageElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы авторизации не найден");
        }

    }

    // Ожидание элемента главной страницы ЛК
    public void findCabinetElement(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(cabinetElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент главной страницы ЛК не найден");
        }

    }

    // Авторизация в ЛК: user rm@cometrica.ru
    public void authorization() throws InterruptedException {

        CommonData page = new CommonData();

        driver.findElement(userName).clear();
        driver.findElement(userName).sendKeys(page.userEmail);
        driver.findElement(userPassword).clear();
        driver.findElement(userPassword).sendKeys(page.userPass);

        sleep(1000);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(submit)).click();

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(cabinetElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент главной страницы ЛК не найден");
        }

    }

    // Авторизация в ЛК: user rm+22@33slona.ru
    public void authorizationSecond() throws InterruptedException {

        AuthorizationPage auth2 = new AuthorizationPage(driver);
        CommonData user = new CommonData();

        driver.findElement(auth2.userName).clear();
        driver.findElement(auth2.userName).sendKeys(user.secondUserEmail);
        driver.findElement(auth2.userPassword).clear();
        driver.findElement(auth2.userPassword).sendKeys(user.secondUserPass);

        sleep(1000);

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(submit)).click();

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(cabinetElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент главной страницы ЛК не найден");
        }

    }

    // Авторизация в ЛК: user rm+33@33slona.ru
    public void authorizationThird() throws InterruptedException {

        CommonData user = new CommonData();

        driver.findElement(userName).clear();
        driver.findElement(userName).sendKeys(user.thirdUserEmail);
        driver.findElement(userPassword).clear();
        driver.findElement(userPassword).sendKeys(user.thirdUserPass);

        sleep(1000);

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(submit)).click();

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(cabinetElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент главной страницы ЛК не найден");
        }

    }

    // Авторизация в ЛК: user rm+44@33slona.ru
    public void authorizationFourth() throws InterruptedException {

        CommonData user = new CommonData();

        driver.findElement(userName).clear();
        driver.findElement(userName).sendKeys(user.fourthUserEmail);
        driver.findElement(userPassword).clear();
        driver.findElement(userPassword).sendKeys(user.fourthUserPass);

        sleep(1000);

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(submit)).click();

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(cabinetElement));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент главной страницы ЛК не найден");
        }

    }

    // Авторизация в Админке: user admin@example.com
    public void authorizationInAdminPanel() throws InterruptedException {

        RentObjectAdminPanelPage page = new RentObjectAdminPanelPage(driver);
        CommonData user = new CommonData();

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(page.submit));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент страницы авторизации не найден");
        }

        driver.findElement(page.userName).clear();
        driver.findElement(page.userName).sendKeys(user.adminUserEmail);
        driver.findElement(page.userPassword).clear();
        driver.findElement(page.userPassword).sendKeys(user.adminUserPass);

        sleep(1000);

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(page.submit)).click();

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(page.filter));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент строки поиска в списке апартаментов не найден");
        }

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(page.objectNumber));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент апартамента в списке апартаментов не найден");
        }

    }

}
