package personalAccount.pageObjects;

import org.openqa.selenium.By;

public class PromosOfPartnersPage {

    public By promosFromPartners = By.xpath("//p[contains(text(),'Акции')]");

    public By promosFromPartnersElement = By.xpath("//h1[text()='Акции от партнеров']");

    public By promo = By.xpath("//div[@class='spec-list']/a[1]");

    public By promoElement = By.xpath("//h4[@class='spec-card__option-title ng-binding'][text()='Описание']");

    public By breadcrumbsPromosOfPartners = By.xpath("//a[@class='link--green ng-binding'][text()='Акции от партнеров']");

}
