package personalAccount.pageObjects;

import org.openqa.selenium.By;

public class ConsumptionHistoryPage {

    public By consumptionHistory = By.xpath("//div[contains(@ui-sref,'consHistory')]/p");

    public By consumptionHistoryElement = By.xpath("//h1[text()='История потребления']");

    public By electric = By.xpath("//a[1][@class='payments_filter-item ng-binding ng-scope']");

    public By coldWater = By.xpath("//a[2][@class='payments_filter-item ng-binding ng-scope']");

    public By hotWater = By.xpath("//a[3][@class='payments_filter-item ng-binding ng-scope']");

    public By monthData = By.xpath("//div[@class='payments_table-container ng-scope']/div[1]/div[1]/div[1]/p");

    public By metersData = By.xpath("//div[@class='payments_table-container ng-scope']/div[1]/div[1]/div/label");

    public By monthDataOfGraph = By.xpath("//div[@class='histogram_bars-wrapper']/div[1]/div[1]/div[1]");

    public By metersDataOfGraph = By.xpath("//div[@class='histogram_bars-wrapper']/div[1]/div[1]/div[2]");

}
