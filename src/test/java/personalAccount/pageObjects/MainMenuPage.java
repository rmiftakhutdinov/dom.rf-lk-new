package personalAccount.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class MainMenuPage {

    public By menu = By.xpath("//div[@class='lk-header_nav']");

    public By closeMenu = By.xpath("//img[@style='cursor: pointer']");

    public By logo = By.xpath("//a[@ui-sref='apartmentsCards']");

    public By apartment = By.xpath("//a[contains(text(),'Апартамент')]");

    public By parking = By.xpath("//a[contains(text(),'Машиноместо')]");

    public By apart250 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'250')]");

    public By parkingA70 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'A70')]");

    public By nextObject = By.xpath("//img[@src='./images/appartment-arrow-right@2x.png']");

    public By helpAndContacts = By.xpath("//a[contains(text(),'Помощь и контакты')]");

    public By helpAndContactsElement = By.xpath("//h1[text()='Помощь и контакты']");

    public By promosFromPartners = By.xpath("//a[contains(text(),'Акции от партнеров')]");

    public By promosFromPartnersElement = By.xpath("//h1[text()='Акции от партнеров']");

    public By myOrders = By.xpath("//a[contains(text(),'Мои заказы')]");

    public By myOrdersElement = By.xpath("//h1[text()='Мои заказы']");

    public By rentParking = By.xpath("//a[contains(text(),'Аренда машиноместа')]");

    public By rentParkingElement = By.xpath("//h1[text()='Аренда машиноместа']");

    public By orderCleaning = By.xpath("//a[contains(text(),'Заказать уборку')]");

    public By orderCleaningElement = By.xpath("//h1[text()='Заказ уборки']");

    public By orderRepair = By.xpath("//a[contains(text(),'Заказать ремонт')]");

    public By orderRepairElement = By.xpath("//h1[text()='Заказ ремонта']");

    public By orderPassesAndKeys = By.xpath("//a[contains(text(),'Пропуска и ключи')]");

    public By orderPassesAndKeysElement = By.xpath("//h1[text()='Заказ пропусков и ключей']");

    public By orderOtherServices = By.xpath("//a[contains(text(),'Прочие услуги')]");

    public By orderOtherServicesElement = By.xpath("//h1[text()='Заказ прочих услуг']");

    public By invoices = By.xpath("//a[contains(text(),'Счета к оплате')]");

    public By invoicesElement = By.xpath("//h1[text()='Счета к оплате']");

    public By paymentHistory = By.xpath("//a[contains(text(),'История платежей')]");

    public By paymentHistoryElement = By.xpath("//h1[text()='История платежей']");

    public By consumptionHistory = By.xpath("//a[contains(text(),'История потребления')]");

    public By consumptionHistoryElement = By.xpath("//h1[text()='История потребления']");

    public By residencePassport = By.xpath("//a[contains(text(),'Паспорт жилья')]");

    public By residencePassportElement = By.xpath("//h2[@class='passport-title ng-binding ng-scope'][text()='Полезная информация']");

    public By conditionsAndContract = By.xpath("//a[contains(text(),'Условия и договор')]");

    public By conditionsAndContractElement = By.xpath("//h1[text()='Условия и договор']");

    public By newsAndNotices = By.xpath("//a[contains(text(),'Новости и уведомления')]");

    public By newsAndNoticesElement = By.xpath("//news-list/div/h1");

    public By orderFoodFromRestaurant = By.xpath("//a[contains(text(),'Заказ еды из ресторана')]");

    public By orderFoodFromRestaurantElement = By.xpath("//h2[@class='food-delivery-modal_title']");

    public By closeFoodModal = By.xpath("//div[@class='lk-modal-close']");

    public By spinner = By.xpath("By.xpath(//div[@class='app-loader ng-scope']");


    // Инициализация драйвера
    private WebDriver driver;
    public MainMenuPage(WebDriver driver){
        this.driver = driver;
    }

    // Поиск элемента закрытия меню
    public void findCloseMenu(){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(closeMenu));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент кнопки закрытия меню не найден");
        }

    }

    // Проверка на видимость логотипа
    public void logoIsDisplayed(){

        try {
            Assert.assertTrue(driver.findElement(logo).isDisplayed());
        } catch (AssertionError ex){
            throw new TimeoutException("Элемент логотипа не найден");
        }

    }

    // Ожидание элемента спиннера и его исчезновения
    public void findSpinner(){

        if(spinnerIsNotPresent(spinner)){
            spinnerIsNotVisible();
        } else {
            new WebDriverWait(driver, 2).until(ExpectedConditions.visibilityOfElementLocated(spinner));
        }

    }

    // Ожидание исчезновения элемента спиннера
    private void spinnerIsNotVisible(){

        try {
            new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOfElementLocated(spinner));
        } catch (TimeoutException ex){
            throw new TimeoutException("Элемент спиннера не исчез");
        }

    }

    // Проверка отсутствия спиннера
    private boolean spinnerIsNotPresent(By selector){

        try{
            new WebDriverWait(driver, 2).until(ExpectedConditions.presenceOfElementLocated(selector));
            return false;
        } catch (TimeoutException ex){
            return true;
        }

    }

}
