package personalAccount.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RentObjectAdminPanelPage {

    public By userName = By.xpath("//input[@name='username qa-login']");

    public By userPassword = By.xpath("//input[@name='password qa-password']");

    public By submit = By.xpath("//button");

    public By filter = By.xpath("//div[@class='search-input-wrapper multi-filter-wrapper ng-scope search-filter']");

    public By filterInput = By.xpath("//input[@placeholder='№ объекта, телефон, арендатор']");

    public By apart250 = By.xpath("//a[@href='/admin/realty-card/apartment/532?n=250']");

    public By contracts = By.xpath("//a[text()='Договоры']");

    public By changeStatus = By.xpath("//a[contains(text(),'Изменить статус')]");

    public By statusRented = By.xpath("//b[text()='Арендован']");

    public By save = By.xpath("//a[text()='Сохранить']");

    public By finishContract = By.xpath("//a[text()='Завершить...']");

    public By finishContractConfirm = By.xpath("//a[text()='Завершить']");

    public By labelStatusRentedApartment = By.xpath("//span[@class='label label-success ng-binding'][contains(text(),'Арендован')]");

    public By labelStatusRentedParking = By.xpath("//span[@class='label ng-binding'][contains(text(),'Арендован')]");

    public By labelStatusFreeApartment = By.xpath("//span[@class='label label-success ng-binding'][contains(text(),'Свободен')]");

    public By labelStatusFreeParking = By.xpath("//span[@class='label ng-binding'][contains(text(),'Свободен')]");

    public By settlementElement = By.xpath("//h1[contains(text(),'Заселение')]");

    public By settleModalConfirm = By.xpath("//h4[text()='Заселение арендатора']");

    public By settleConfirm = By.xpath("//a[contains(text(),'Заселить')]");

    public By rentModalConfirm = By.xpath("//h4[text()='Сдача машиноместа']");

    public By rentConfirm = By.xpath("//a[contains(text(),'Сдать')]");

    public By objectNumber = By.xpath("//tbody/tr[1]/td[2]/a");

    public By leaseElement = By.xpath("//h1[contains(text(),'Сдача машиноместа')]");

    public By dropDownRenter = By.xpath("//span[@class='pull-left filter-option']");

    public By inputChoiseRenter = By.xpath("//input[@class='form-control']");

    public By calendar = By.xpath("//input[@id='stlmDP2']");

    public By nextMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[3]");

    public By setDate = By.xpath("//div[@class='double-input-group']/div[2]//div[@class='moment-picker-specific-views']/table/tbody/tr[2]/td[1]");

    public By inputIndication = By.xpath("//input[@id='input13_0']");

    public By indication = By.xpath("//button[@title='Скопировать в поле ввода']");

    public By nextStep = By.xpath("//button[contains(text(),'Далее')]");

    public By nextStepApartmentElement = By.xpath("//h1[contains(text(),'Подтверждение заселения')]");

    public By nextStepParkingElement = By.xpath("//h1[contains(text(),'Подтверждение сдачи')]");

    public By settle = By.xpath("//button[@type='submit']");

    public By lease = By.xpath("//button[contains(text(),'Сдать')]");

    public By ejectConfirm = By.xpath("//a[text()='Выселить']");

    public By settleModal = By.xpath("//div[@id='cant_moveout_modal']/div[@class='modal-dialog modal-sm']");

    public By checkboxCall = By.xpath("//div[@id='cant_moveout_modal']//div[@class='modal-body']//div[1]//input");

    public By changeObjectType = By.xpath("//select[@ng-change='changeObjType()']");

    public By parkingA70 = By.xpath("//td/a[@href='/admin/realty-card/parking/575?n=A70']");

    public By addPhoto = By.xpath("//input[@id='input25']");

    public By renterInput202 = By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]");

    public By leaveAccess = By.xpath("//div[@id='done_modal']//div[@class='checkbox']//input");

    public By leaveAccessUS = By.xpath("//div[@id='cant_moveout_modal']//div[@class='modal-body']//div[2]//input");

    public String apartmentNumber250 = "250";

    public String parkingNumberA70 = "A70";

    public String renterID = "202";


    // Инициализация драйвера
    private WebDriver driver;
    public RentObjectAdminPanelPage(WebDriver driver){
        this.driver = driver;
    }

}
